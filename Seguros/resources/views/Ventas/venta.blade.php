@extends('layout.app')

@section('content')
     <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                    <a href="/">LAB-Palacio Alcivar</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{{Session::get('usuario')}}</div>
                                <div class="profile-data-title">{{Session::get('nombres_apellidos')}}</div>
                            </div>
  
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    @if(Session::get('rol')==1)
                    <li>
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                        <ul>
                            <li><a href="/usuarios">Administrar Usuario</a></li>
                            <li><a href="/roles">Administrar Roles</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                    @elseif(Session::get('rol')==2)
                     <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                     @elseif(Session::get('rol')==13)
                     <li>
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                                       
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>   
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Ventas</a></li>                    
                    <li class="active">Venta</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                      <div class="btn-group" role="group" aria-label="...">
                                        <a target="_BLANK" href="/ventas/reporte/downloadExcelVenta/csv" class="btn btn-default"><i class="fa fa-file-excel-o"></i> Exportar a Excel</a>
                                        <!--<a target="_BLANK" href="#" class="btn btn-default"><i class="fa fa-download"></i> Exportar a Pdf</a>-->
                                    </div>
                                    <ul class="panel-controls">
                                        <!--  
                                        <li><a href="#" id="btn_exportar"><span class="fa fa-plus"></span></a></li>
                                        -->
                                        
                                         <li><a href="#" id="btn_new_venta"><span class="fa fa-plus"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body" id="tabla_usuarios">
                                    <table class="table datatable" >
                                        <thead>
                                            <tr>
                                                <th>NUMERO IDENTIFICACIÓN</th>
                                                <th>NOMBRES</th>
                                                <th>FECHA</th>
                                                <th>MES INICIO</th>
                                                <th>TOTAL</th>
                                                <th>OPCIONES</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         @foreach($ventas as $v)
                                            <tr>
                                                <td>{{$v->numero_identificacion}}</td>
                                                <td>{{$v->nombres}}</td>
                                                <td>{{$v->fecha_adicion}}</td>
                                                <td>{{$v->mes_anio_inicio}}</td>
                                                <td>{{$v->total_acumulado}}</td>
                                                <td><div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Opciones <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="/ventas/update/{{$v->numero_identificacion}}">Modificar</a></li>
                                                        <li><a href="#" onclick="add_dependientes({{$v->numero_identificacion}})">Agregar dependientes</a></li>
                                                    </ul>
                                                </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                        </div>
                    </div>                      
                   
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

<div class="modal fade" id="modal_agregar_dependientes" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Agregar dependientes</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <input type="hidden" id="cedula_titular" >
            <div class="col-md-12">
                                    <label for="">Identificación*</label>
                                    <input type="text" id="identificacion_dep" class="form-control">
                                    <label for="">Nombres*</label>
                                    <input type="text" class="form-control" id="nombres_dep"  style="text-transform: uppercase;"  onkeyup="javascript:this.value=this.value.toUpperCase()" >
                                    <div class="col-md-4 col-4-m">
                                        <label for="">Fecha Nacimiento*</label>
                                    <input type="text" class="form-control" id="txt-date" placeholder="fecha de nacimiento" data-date-format="yyyy-mm-dd"/>
                                    </div>
                                    <div class="col-md-4 col-4-m">
                                     <label for="">Género*</label>
                                    <select id="genero_dep" class="form-control">
                                        <option value="0">Seleccione un Género</option>
                                        <option value="F">Femenino</option>
                                        <option value="M">Masculino</option>
                                    </select>
                                    </div>
                                    <div class="col-md-4 col-4-m">
                                         <label for="">Parentesco*</label>
                                        <select id="parentesco" class="form-control">
                                            <option value="0">Seleccione un Parentesco</option>
                                            <option value="Esposa">Esposo(a)</option>
                                            <option value="Hijo(a)">Hijo(a)</option>
                                        </select>
                                    </div>
                                    </div>
                                    <div style="text-align: center; padding-bottom: 10px;">
                                        <button type="button" class="btn btn-primary btn-add" id="b_añadir_2">Añadir</button>
                                    </div>
        </div>   
        <div class="row">
            <table id="tabla_dependencia" class="table" >
                <thead>
                    <tr>
                        <th>#</th>
                        <th>IDENTIFICACIÓN</th>
                        <th>NOMBRES</th>
                        <th>FECHA</th>
                        <th>GENERO</th>
                        <th>PARENTESCO</th>
                        <th>:::</th>
                    </tr>
                </thead>
                <tbody id="tbody_dependientes"> </tbody>
            </table>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('js')
<script type="text/javascript" src="{{asset('template/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('template/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script type="text/javascript" src="{{asset('template/js/plugins.js')}}"></script> 

<script type="text/javascript" src="{{asset('js/control_ventas.js')}}"></script>  

<script type="text/javascript">
    $("#txt-date").datepicker({
        autoclose: 'true'

    });
</script>
@endsection
@extends('layout.app')

@section('css')
    <link rel="stylesheet" href="{{asset('plugins/datapicker/css/bootstrap-datetimepicker.min.css')}}">
@endsection

@section('content')
     <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                    <a href="/">LAB-Palacio Alcivar</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{{Session::get('usuario')}}</div>
                                <div class="profile-data-title">{{Session::get('nombres_apellidos')}}</div>
                            </div>

                        </div>                                                                        
                    </li>
                   <li class="xn-title">Navigation</li>
                   @if(Session::get('rol')==1)
                    <li>
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                        <ul>
                            <li><a href="/usuarios">Administrar Usuario</a></li>
                            <li><a href="/roles">Administrar Roles</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                    @elseif(Session::get('rol')==2)
                     <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                     @elseif(Session::get('rol')==13)
                     <li class="active">
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>

                   
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    @endif

                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>   
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
  
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Inicio</a></li>                    
                    <li><a href="#">Venta</a></li>
                    <li class="active">Reporte Ventas</li>
                </ul>
                <!-- END BREADCRUMB -->

                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Reporte de ventas por usuario</h2>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->    

                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START DEFAULT DATATABLE -->
                            <div class="load">
                                <div class=load-container>
                                    <div class="cssload-thecube">
                                        <div class="cssload-cube cssload-c1"></div>
                                        <div class="cssload-cube cssload-c2"></div>
                                        <div class="cssload-cube cssload-c4"></div>
                                        <div class="cssload-cube cssload-c3"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title"></h3>
                                    <div class="btn-group" role="group" aria-label="...">
                                        <label for="">Seleccione un usuario</label>
                                       <select id="CBM_USUARIOS" class="form-control">
                                           <option value="0">Todos los usuarios</option>
                                           @foreach($usuario as $u)
                                            <option value="{{$u->usuario}}">{{$u->usuario}}</option>
                                            option
                                           @endforeach
                                       </select>
                                    </div>                             
                                        <div class="pull-right box-tools" style="display:  flex; ">
                                            <div id="daterange-btn" class="pull-right" style="background: #fff; cursor: pointer; padding: 10px; border: 1px solid #ccc; width: auto">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                            <span id="fechas"></span> <b class="caret"></b>
                                            </div>
                                        <button type="button" id="B_FILTRAR" class="btn btn-info"><i class="fa fa-search"></i> filtrar</button>
                                        </div><!-- /. tools -->


                                        
                                </div>
                                <div class="panel-body" id="tabla_cliente">
                                    <table class="table datatable" >
                                        <thead>
                                            <tr>
                                                <th>USUARIO</th>
                                                <th>VENTAS DE HOY</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($ventas2 as $v)
                                            <tr>
                                            <td>{{$v->usuario_adicion}}</td>
                                            <td>{{$v->sum}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                       
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                        </div>
                    </div>                            
                </div>
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->


@endsection

@section('js')
<script type="text/javascript" src="{{asset('template/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script type="text/javascript" src="{{asset('template/js/plugins.js')}}"></script>  
<script type="text/javascript" src="{{asset('js/control_clientes_admin.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/moment.min.js')}}"></script>
<script src="{{asset('plugins/datapicker/js/bootstrap-datetimepicker.js')}}"></script>
<!--<script type="text/javascript" src="{{ asset('plugins/datapickerjs/daterangepicker.js') }}"></script>-->
<script type="text/javascript">
  var fecha ="";
var fecha2 ="";

$(document).ready(function(){
 var start = moment();
    var end = moment();
    cb(start, end);
})

function cb(start, end){
   $('#daterange-btn span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
   fecha =(start.format('YYYY-MM-DD'));
     fecha2 =(end.format('YYYY-MM-DD'));
}
$(function () {
$('#daterange-btn').daterangepicker(
    {
      ranges: {
        'Hoy': [moment(), moment()],
        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Ultimos 7 Días': [moment().subtract(6, 'days'), moment()],
        'Ultimos 30 Días': [moment().subtract(29, 'days'), moment()],
        'Este mes': [moment().startOf('month'), moment().endOf('month')],
        'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment(),
      endDate: moment()
    },
function (start, end) {
  $('#daterange-btn span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
     fecha =(start.format('YYYY-MM-DD'));
     fecha2 =(end.format('YYYY-MM-DD'));

  }
);
});


$("#B_FILTRAR").click(function(){
    var usuarios = $("#CBM_USUARIOS").val();
    if(usuarios == 0){
        $(".load").show();
        get_ventas_todos(fecha,fecha2);
    }else{
        $(".load").show();
         get_venta_usuario(usuarios,fecha,fecha2)
    }
})

function get_venta_usuario(usuario,fecha1,fecha2){
    var tabla="<table class='table datatable'> <thead> <tr> <th>IDENTIFICACION</th> <th>NOMBRES</th><th>CODIGO CUENTA</th><th>FECHA CREACION</th><th>MES INICIO</th><th>MES COBRADO</th><th>USUARIO</th> </tr> </thead> <tbody>"

    $.ajax({
        url:'/get_ventas_usuario/'+usuario+'/'+fecha1+'/'+fecha2,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                tabla +="<tr>";
                tabla +="<td>"+v.numero_identificacion+"</td>";
                tabla +="<td>"+v.nombres+"</td>";
                tabla +="<td>"+v.codigo_cuenta+"</td>";
                tabla +="<td>"+v.fecha_adicion+"</td>";
                tabla +="<td>"+v.mes_anio_inicio+"</td>";
                tabla +="<td>"+v.ultimo_mes_cobrado+"</td>";
                tabla +="<td>"+v.usuario_adicion+"</td>";
                tabla +="</td> </tr>";
            });
            tabla +="</tbody> </table>";

            $("#tabla_cliente").html(tabla);
            $(".datatable").dataTable();
            $(".load").hide();
        }
    })
}
function get_ventas_todos(fecha1,fecha2){
    var tabla="<table class='table datatable'> <thead> <tr> <th>USUARIO</th> <th>VENTAS REALIZADAS</th> </tr> </thead> <tbody>"

    $.ajax({
        url:'/get_ventas_todos/'+fecha1+'/'+fecha2,
        type:'GET',
        dataType:'json',
        success:function(res){
            $(res).each(function(i, v){
                tabla +="<tr>";
                tabla +="<td>"+v.usuario_adicion+"</td>";
                tabla +="<td>"+v.sum+"</td>";
                tabla +="</td> </tr>";
            });
            tabla +="</tbody> </table>";

            $("#tabla_cliente").html(tabla);
            $(".datatable").dataTable();
            $(".load").hide();

        }
    })
}
</script>
@endsection
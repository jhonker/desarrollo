@extends('layout.app')

@section('content')
     <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                    <a href="/">LAB-Palacio Alcivar</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{{Session::get('usuario')}}</div>
                                <div class="profile-data-title">{{Session::get('nombres_apellidos')}}</div>
                            </div>
              
                        </div>                                                                        
                    </li>
                    <li class="xn-title">Navigation</li>
                    @if(Session::get('rol')==1)
                    <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                        <ul>
                            <li><a href="/usuarios">Administrar Usuario</a></li>
                            <li><a href="/roles">Administrar Roles</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                    @elseif(Session::get('rol')==2)
                     <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                     @elseif(Session::get('rol')==13)
                     <li>
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>

                   
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    @endif
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>   
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->

                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     

                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="/ventas">Listado Ventas</a></li>                    
                    <li class="active">Actualizar venta</li>
                </ul>
                <!-- END BREADCRUMB -->                       
                
                <!-- PAGE CONTENT WRAPPER -->
                <div class="page-content-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    @foreach($cliente as $c)
                                    <label class="control-label">Cliente:</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="nombres" value="{{$c->cliente}}" />
                                        <span class="input-group-addon mouse" id="btn_buscar_cliente">
                                            <span class="fa fa-search"> Buscar cliente</span>
                                        </span>
                                        
                                    </div> 
                                    <div class="col-12" style="display:none;  height:150px; overflow-x: scroll;" id="div-search-hd"></div>
                                    <div id="info" class="row" style="margin-top:  16px;">
                                        <div class="col-md-6">
                                            <strong><label for="">Codigo:</label></strong>
                                            <span id="codigo">{{$c->codigo_cliente}}</span><br>
                                            <strong><label for="">Identificación:</label></strong>
                                            <span id="identificacion">{{$c->numero_identificacion}}</span><br>
                                            <strong><label for="">Telefono:</label></strong>
                                            <span id="sp_telefono"><span id="telefono">{{$c->telefono}}</span></span><br>
                                            <strong><label for="">Celular:</label></strong>
                                            <span id="sp_celular"><span id="celular">{{$c->celular}}</span></span><br>
                                            <strong><label for="">Correo:</label></strong>
                                            <span id="sp_correo"><span id="correo">{{$c->correo_electronico}}</span>
                                            <input type="hidden" id="c_cuenta" value="{{$c->codigo_cuenta}}">
                                            </span>
                                            @endforeach
                                            <br>
                                            <i class="fa fa-pencil fa-lg" id="edit_datos" aria-hidden="true"></i>
                                            <i class="fa fa-floppy-o fa-lg"  id="save_datos" style="display: none" aria-hidden="true"></i>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="cuentas">Cuentas:</label>
                                            <select id="cbm_cuentas" class="form-control">
                                                <option value="0">Selecciones la cuenta</option>
                                            </select>
                                             <strong><label for="">Tipo Cuenta:</label></strong>
                                            <span id="tipo_cuenta"></span>
                                            <input type="hidden" id="codigo_cuenta" value="">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                           <div class="panel panel-default">
                                <div class="panel-body col-md-12">
                                    <label for="">Seguro *</label>
                                        <select id="cbm_seguros" class="form-control">
                                            <option value="0">Selecciones un seguro</option>
                                            @foreach($seguros as $s)
                                                <option value="{{$s->codigo_seguro}}">{{$s->descripcion}}</option>
                                            @endforeach
                                        </select>
                                    <div class="col-md-12">
                                    <label for="">Precio*</label>
                                    <input type="text" class="form-control" disabled="true" id="precio_seguro">
                                    </div>
                                   
                                </div>
                            </div>

                            <div class="panel panel-default">
                                 <div class="panel-heading">                                
                                    <h3 class="panel-title">Dependencias</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="col-md-12">
                                    <label for="">Identificación*</label>
                                    <input type="text" id="identificacion_dep" class="form-control">
                                    <label for="">Nombres*</label>
                                    <input type="text" class="form-control" id="nombres_dep"  style="text-transform: uppercase;"  onkeyup="javascript:this.value=this.value.toUpperCase()" >
                                    <div class="col-md-4 col-4-m">
                                        <label for="">Fecha Nacimiento*</label>
                                    <input type="text" class="form-control" id="txt-date" placeholder="fecha de nacimiento" data-date-format="yyyy-mm-dd"/>
                                    </div>
                                    <div class="col-md-4 col-4-m">
                                     <label for="">Género*</label>
                                    <select id="genero_dep" class="form-control">
                                        <option value="0">Seleccione un Género</option>
                                        <option value="F">Femenino</option>
                                        <option value="M">Masculino</option>
                                    </select>
                                    </div>
                                    <div class="col-md-4 col-4-m">
                                         <label for="">Parentesco*</label>
                                        <select id="parentesco" class="form-control">
                                            <option value="0">Seleccione un Parentesco</option>
                                            <option value="Esposa">Esposo(a)</option>
                                            <option value="Hijo(a)">Hijo(a)</option>
                                        </select>
                                    </div>
                                    </div>
                                    <div>
                                        <button type="button" class="btn btn-primary btn-add" id="b_añadir">Añadir</button>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="col-md-12">
                                        <label for=""></label>
                                        <table id="tabla_dependencia" class="table" >
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>IDENTIFICACIÓN</th>
                                                <th>NOMBRES</th>
                                                <th>FECHA</th>
                                                <th>GENERO</th>
                                                <th>PARENTESCO</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_dependientes">
                                            <?php $cont=1 ?>
                                            @foreach($dependientes as $d)
                                            <tr><td>{{$cont}}</td>
                                                <td>{{$d->numero_identificacion}}</td>
                                                <td>{{$d->nombres}}</td>
                                                <td>{{$d->fecha_nacimiento}}</td>
                                                <td>{{$d->genero}}</td>
                                                <td>{{$d->parentezco}}</td>
                                                <td><a class='elimina' onclick='deleteDependiente({{$d->codigo_cliente_dependiente}})'><img class='delete' src='/dist/remove.png' /></a></td>
                                            </tr>
                                            <?php $cont= $cont+1; ?>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                    </div> 
                    <div class="row col-md-8" style="display:  flex; justify-content:  center; ">
                        <button class="btn-info btn" type="button" id="btn_update_venta">Actualizar Venta</button>
                    </div>
                     
                   
                </div>
                <!-- END PAGE CONTENT WRAPPER -->                                
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->


<div class="modal fade" id="modal_cliente_search" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Buscar Usuario</h4>
      </div>
      <div class="modal-body">
        <div class="row">
           <div id="tabla_cliente"></div>
        </div>   
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('js')
<script type="text/javascript" src="{{asset('template/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script type="text/javascript" src="{{asset('template/js/plugins.js')}}"></script> 
<script src="{{asset('plugins/fullcalendar/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/control_ventas_update.js')}}"></script>  
<script type="text/javascript">
    $("#txt-date").datepicker({
        autoclose: 'true'

    });
</script>
@endsection
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <title>Document</title>
</head>
<style>
td.details-control {
    background: url('../img/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.details td.details-control {
    background: url('../img/details_close.png') no-repeat center center;
}

.opciones{
    cursor:pointer;
}
</style>
<body>
    <table class="display" id="myTable">
    <thead>
        <th>ID</th>
        <th>ID</th>
        <th>LATITUD</th>
        <th>LONGITUD</th>
    </thead>
    <tbody>
         @foreach($dispositivo as $d)
            <tr>
                <td></td>
                <td>{{$d->id}}</td>
                <td>{{$d->id_movil}}</td>
                <td>{{$d->numero}}</td>
            </tr>
         @endforeach
    </tbody>
    </table>

<div class="modal fade"  id="modal_opciones" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="id_cliente"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <h5>Opcion de modificar datos del cliente</h5>
      <button type="button" onclick= "modificar_cliente()" class="btn btn-info">Modificar</button>
      <hr>
      <button type="button" class="btn btn-info">Eliminar</button>
      </div>
     
    </div>
  </div>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<script>

function format ( d ) {
    return '<img class="opciones" onclick="modificar_cliente('+d.id+')" src="img/edit.png">'
        +'<img class="opciones" onclick="modificar_cliente('+d.id+')" src="img/delete.png">';
}
 
$(document).ready(function() {
   // $('#myTable').DataTable({
    var dt = $('#myTable').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "api/dispositivos",
        "columns": [
            {
                "class":          "details-control",
                "orderable":      false,
                "data":           null,
                "defaultContent": ""
            },
            {data:'id'},
            {data:'id_movil'},
            {data:'numero'}
        ],
        "order": [[1, 'asc']]
    } );

     // Array to track the ids of the details displayed rows
     var detailRows = [];
 
 $('#myTable tbody').on( 'click', 'tr td.details-control', function () {
     var tr = $(this).closest('tr');
     var row = dt.row( tr );
     var idx = $.inArray( tr.attr('id'), detailRows );

     if ( row.child.isShown() ) {
         tr.removeClass( 'details' );
         row.child.hide();

         // Remove from the 'open' array
         detailRows.splice( idx, 1 );
     }
     else {
         tr.addClass( 'details' );
         console.log(row.data());
         row.child( format( row.data() ) ).show();

         // Add to the 'open' array
         if ( idx === -1 ) {
             detailRows.push( tr.attr('id') );
         }
     }
 } );

 // On each draw, loop over the `detailRows` array and show any child rows
 dt.on( 'draw', function () {
     $.each( detailRows, function ( i, id ) {
         $('#'+id+' td.details-control').trigger( 'click' );
     } );
 } );
 
 
} );
$(document).ready( function () {


    /*var selected = [];
    $('#myTable').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": "api/dispositivos",
        "rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
        },
        "columns":[
            {data:'id'},
            {data:'id_movil'},
            {data:'numero'},
            ]
    });
    $('#myTable tbody').on('click', 'tr', function () {
        var id = this.id;
        var dato = $(this).find('td:first').html();
        var index = $.inArray(id, selected);
        //alert(dato);
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
        $("#modal_opciones").modal("show");
        $("#id_cliente").html(dato);
        //$(this).toggleClass('selected');
    } );*/

} );

function modificar_cliente(id){
    $("#modal_opciones").modal("show");
    $("#id_cliente").html(id);
}

function clic(){
 
}

</script>
</html>
@extends('layout.app')

@section('css')
    <link rel="stylesheet" href="{{asset('plugins/datapicker/css/bootstrap-datetimepicker.min.css')}}">
    <!--<link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">-->
    <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">


    <style type="text/css" media="screen">
        td.details-control {
            background: url('../img/details_open.png') no-repeat center center;
            cursor: pointer;
        }
        tr.details td.details-control {
            background: url('../img/details_close.png') no-repeat center center;
        }
        .opciones{
            cursor:pointer;
        }
    </style>
@endsection

@section('content')
     <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                    <a href="/">LAB-Palacio Alcivar</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{{Session::get('usuario')}}</div>
                                <div class="profile-data-title">{{Session::get('nombres_apellidos')}}</div>
                            </div>

                        </div>                                                                        
                    </li>
                   <li class="xn-title">Navigation</li>
                   @if(Session::get('rol')==1)
                    <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                        <ul>
                            <li><a href="/usuarios">Administrar Usuario</a></li>
                            <li><a href="/roles">Administrar Roles</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                    @elseif(Session::get('rol')==2)
                     <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                     @elseif(Session::get('rol')==13)
                     <li class="active">
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>

                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    @endif

                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>   
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
  
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Inicio</a></li>                    
                    <li><a href="#">Clientes</a></li>
                    <li class="active">Administración de Clientes</li>
                </ul>
                <!-- END BREADCRUMB -->

                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Tabla de Clientes</h2>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->    

                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title"></h3>
                                    <div class="btn-group" role="group" aria-label="...">
                                        <a target="_BLANK" href="clientes/downloadExcelClient/xlsx" class="btn btn-default"><i class="fa fa-download"></i> Exportar a Excel</a>
                                        <a target="_BLANK" href="clientes/downloadPdfClient" class="btn btn-default"><i class="fa fa-download"></i> Exportar a Pdf</a>
                                        
                                        <button type="button" id="btn_new_client" class="btn btn-default"><i class="fa fa-plus"> </i> Agregar Clientes Excel</button>
                                        <button type="button" id="btn_client" class="btn btn-default"><i class="fa fa-plus"> </i> Agregar Cliente</button>
                                    </div>                             
                                </div>
                                <div class="panel-body">
                                    <table class="display" id="tabla_cliente">
                                        <thead>
                                                <th>CODIGO</th>
                                                <th>CODIGO</th>
                                                <th>IDENTIFICACIÓN</th>
                                                <th>NOMBRES</th>
                                                <th>FECHA NACIMIENTO</th>
                                                <th>TELEFONO</th>
                                                <th>CELULAR</th>
                                                <th>CIUDAD</th>
                                                <th>DIRECCION</th>
                                        </thead>
                                        <tbody>
                                            @foreach($client as $r)
                                            <tr>
                                                <td></td>
                                                <td>{{$r->codigo_cliente}}</td>
                                                <td>{{$r->numero_identificacion}}</td>
                                                <td>{{$r->nombres}}</td>
                                                <td>{{$r->fecha_nacimiento}}</td>
                                                <td>{{$r->telefono}}</td>
                                                <td>{{$r->celular}}</td>
                                                <td>{{$r->ciudad}}</td>
                                                <td>{{$r->direccion}}</td>
                                                
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                        </div>
                    </div>                            
                </div>
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!--MODAL -->
            <div class="modal" id="modal_client" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="defModalHead">Cargar Nuevos Clientes</h4>
                    </div>
                    <div class="modal-body clearfix">
                        <form  action="clientes/importarExcelClientes" class="form-horizontal clearfix" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
                            <input type="file" name="import_file" />

                            <button style="margin-top:10px" class="btn btn-primary pull-right">Import File</button>

                        </form>
                    </div>
                    <div class="modal-footer clearfix">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--FIN DEL MODAL-->

        <!--MODAL -->
        <div class="modal" id="modal_client_new" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="defModalHead">Agregar Nuevo Cliente</h4>
                    </div>
                    <div class="modal-body clearfix">
                        <table class="table table-responsive">
                            <tr>
                                <td>Código Identificación:</td>
                                <td><input type="text" class="form-control" id="txt-codigo" placeholder="código" /></td>
                            </tr>
                            <tr>
                                <td>Nombres y Apellidos:</td>
                                <td><input type="text" class="form-control" id="txt-names" placeholder="nombres y apellidos" /></td>
                            </tr>
                            <tr>
                                <td>Fecha de Nacimiento:</td>
                                <td><input type="text" class="form-control" id="txt-date" placeholder="fecha de nacimiento" data-date-format="yyyy-mm-dd"/>
                               </td>
                            </tr>
                            <tr>
                                <td>Teléfono:</td>
                                <td><input type="text" class="form-control" id="txt-telefono" placeholder="teléfono convencional" /></td>
                            </tr>
                            <tr>
                                <td>Correo:</td>
                                <td><input type="text" class="form-control" id="txt-correo" placeholder="correo" /></td>
                            </tr>
                            <tr>
                                <td>Celular:</td>
                                <td><input type="text" class="form-control" id="txt-celular" placeholder="celular" /></td>
                            </tr>
                            <tr>
                                <td>Provincia:</td>
                                <td><input type="text" class="form-control" id="txt-provincia" placeholder="provincia" /></td>
                            </tr>
                            <tr>
                                <td>Ciudad:</td>
                                <td><input type="text" class="form-control" id="txt-ciudad" placeholder="ciudad" /></td>
                            </tr>
                            <tr>
                                <td>Dirección:</td>
                                <td><input type="text" class="form-control" id="txt-direccion" placeholder="direccion" /></td>
                            </tr>
                            <tr>
                                <td>Genero:</td>
                                <td>
                                    <select id="txt-genero" class="form-control">
                                        <option value="M">Masculino</option>
                                        <option value="F">Femino</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Tipo Cuenta:</td>
                                <td>
                                    <select id="txt-tipo-cuenta" class="form-control">
                                        <option value="CUENTA AHORRO NORMAL">Cuenta Ahorro</option>
                                        <option value="CUENTA CORRIENTE NORMAL">Cuenta Corriente</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Cuenta:</td>
                                <td><input type="text" class="form-control" id="txt-cuenta" placeholder="cuenta" /></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer clearfix">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-success" id="btn-save-client">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--FIN DEL MODAL-->

        <!--MODAL -->
        <div class="modal" id="modal_client_update" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="defModalHead">Modificar Cliente</h4>
                    </div>
                    <div class="modal-body clearfix">
                        <div class="col-xs-12" id="div-hd">
                            <h3 class="text-center"><i class="fa fa-spinner fa-2x fa-spin fa-fw"></i></h3>
                        </div>
                        <table class="table table-responsive hidden-dom" id="hidden1">
                            <tr>
                                <td><strong>Cliente Seleccionado:<strong></td>
                                <td id="div-cl"></td>
                            </tr>
                            <tr>
                                <td>Código Identificación:</td>
                                <td><input type="hidden" id="txt-id" /> <input type="text" class="form-control" id="txt-codigo2" placeholder="código" /></td>
                            </tr>
                            <tr>
                                <td>Nombres y Apellidos:</td>
                                <td><input type="text" class="form-control" id="txt-names2" placeholder="nombres y apellidos" /></td>
                            </tr>
                            <tr>
                                <td>Fecha de Nacimiento:</td>
                                <td><input type="text" class="form-control" id="txt-date2" placeholder="fecha de nacimiento" /></td>
                            </tr>
                            <tr>
                                <td>Teléfono:</td>
                                <td><input type="text" class="form-control" id="txt-telefono2" placeholder="teléfono convencional" /></td>
                            </tr>
                            <tr>
                                <td>Correo:</td>
                                <td><input type="text" class="form-control" id="txt-correo2" placeholder="correo" /></td>
                            </tr>
                            <tr>
                                <td>Celular:</td>
                                <td><input type="text" class="form-control" id="txt-celular2" placeholder="celular" /></td>
                            </tr>
                            <tr>
                                <td>Provincia:</td>
                                <td><input type="text" class="form-control" id="txt-provincia2" placeholder="provincia" /></td>
                            </tr>
                            <tr>
                                <td>Ciudad:</td>
                                <td><input type="text" class="form-control" id="txt-ciudad2" placeholder="ciudad" /></td>
                            </tr>
                            <tr>
                                <td>Dirección:</td>
                                <td><input type="text" class="form-control" id="txt-direccion2" placeholder="direccion" /></td>
                            </tr>
                            <tr>
                                <td>Genero:</td>
                                <td>
                                    <select id="txt-genero2" class="form-control">
                                        <option value="M">Masculino</option>
                                        <option value="F">Femino</option>
                                    </select>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div class="modal-footer clearfix hidden-dom" id="hidden2">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <button type="button" class="btn btn-success" id="btn-update-client">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--FIN DEL MODAL-->

@endsection

@section('js')
<!--<script type="text/javascript" src="{{asset('template/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    -->
<script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{asset('template/js/plugins.js')}}"></script>  
<script type="text/javascript" src="{{asset('js/control_clientes_admin.js')}}"></script>
<script src="{{asset('plugins/fullcalendar/moment.min.js')}}"></script>
<!--<script src="{{asset('plugins/datapicker/js/bootstrap-datetimepicker.js')}}"></script>-->
<!--<script type="text/javascript" src="{{ asset('plugins/datapickerjs/daterangepicker.js') }}"></script>-->
<script>

function format ( d ) {
    return '<img class="opciones" onclick="update_cliente('+d.codigo_cliente+')" src="img/edit.png">'
        +'<img class="opciones" onclick="delete_cliente('+d.codigo_cliente+')" src="img/delete.png">';
}
 
$(document).ready(function() {
   // $('#myTable').DataTable({
    var token   = $("#token").val();
    var dt = $('#tabla_cliente').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax":{
            url:'api/clientes',
            type:'POST',
            headers :{'X-CSRF-TOKEN': token},
        },

        "columns": [
            {
                "class":          "details-control",
                "orderable":      false,
                "defaultContent": ""
            },
            {data:'codigo_cliente'},
            {data:'numero_identificacion'},
            {data:'nombres'},
            {data:'fecha_nacimiento'},
            {data:'telefono'},
            {data:'celular'},
            {data:'ciudad'},
            {data:'direccion'}
        ],
        "order": [[1, 'asc']]
    } );

     // Array to track the ids of the details displayed rows
     var detailRows = [];
 
 $('#tabla_cliente tbody').on( 'click', 'tr td.details-control', function () {
     var tr = $(this).closest('tr');
     var row = dt.row( tr );
     var idx = $.inArray( tr.attr('id'), detailRows );

     if ( row.child.isShown() ) {
         tr.removeClass( 'details' );
         row.child.hide();

         // Remove from the 'open' array
         detailRows.splice( idx, 1 );
     }
     else {
         tr.addClass( 'details' );
         console.log(row.data());
         row.child( format( row.data() ) ).show();

         // Add to the 'open' array
         if ( idx === -1 ) {
             detailRows.push( tr.attr('id') );
         }
     }
 } );

 // On each draw, loop over the `detailRows` array and show any child rows
 dt.on( 'draw', function () {
     $.each( detailRows, function ( i, id ) {
         $('#'+id+' td.details-control').trigger( 'click' );
     } );
 } );
 
 
} );
</script>
@endsection
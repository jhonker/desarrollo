@extends('layout.app')

@section('content')
     <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                    <a href="/">LAB-Palacio Alcivar</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                               <div class="profile-data-name">{{Session::get('usuario')}}</div>
                                <div class="profile-data-title">{{Session::get('nombres_apellidos')}}</div>
                            </div>
           
                        </div>                                                                        
                    </li>
                   <li class="xn-title">Navigation</li>
                   @if(Session::get('rol')==1)
                    <li>
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                        <ul>
                            <li><a href="/usuarios">Administrar Usuario</a></li>
                            <li><a href="/roles">Administrar Roles</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                    <li class="xn-openable ">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                    @elseif(Session::get('rol')==2)
                     <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                     @elseif(Session::get('rol')==13)
                     <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>

                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    @endif
                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>   
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->

                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Inicio</a></li>                    
                    <li><a href="#">Usuarios</a></li>
                    <li class="active">Administracion de Usuarios</li>
                </ul>
                <!-- END BREADCRUMB -->

                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Tabla de Usuarios</h2>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->    

                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <h3 class="panel-title"></h3>
                                    <ul class="panel-controls">
                                        <!--<li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>-->
                                        <!--<li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>-->
                                        <li><a href="#" id="btn_new_usuario"><span class="fa fa-plus"></span></a></li>
                                    </ul>                                
                                </div>
                                <div class="panel-body" id="tabla_usuarios">
                                    <table class="table datatable" >
                                        <thead>
                                            <tr>
                                                <th>CODIGO</th>
                                                <th>NOMBRE DE USUARIO</th>
                                                <th>USUARIO</th>
                                                <th>OPCIONES</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         @foreach($user as $usu)
                                            <tr>
                                                <td>{{$usu->codigo_usuario}}</td>
                                                <td>{{$usu->usuario}}</td>
                                                <td>{{$usu->nombres}} {{$usu->primer_apellido}} {{ $usu->segundo_apellido}}</td>
                                                <td><div class="btn-group">
                                                    <a href="#" data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Opciones <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" role="menu">
                                                        <li><a href="#" onclick="update_usuario({{$usu->codigo_usuario}})">Modificar</a></li>
                                                        <li><a href="#" onclick="delete_usuario({{$usu->codigo_usuario}})">Eliminar</a></li>
                                                    </ul>
                                                </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                        </div>
                    </div>                            
                </div>
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <!--MODAL -->
            <div class="modal" id="modal_usuarios" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="defModalHead">Registrar nuevo Usuario</h4>
                    </div>
                    <div class="modal-body">
                         <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Rol:</label>
                                <div class="col-md-5">
                                    <select id="cbm_rol" class="form-control">
                                        <option value="0">Seleccione un Rol</option>
                                        @foreach($roles as $r)
                                        <option value="{{$r->codigo_rol}}">{{$r->rol}}</option>
                                        @endforeach
                                    </select>
                                </div>
                        </div>
                        <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Nombre de Usuario:</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="nombre_usuario" placeholder="Escriba un nombre de usuario"/>
                                </div>
                        </div>
                         <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Nombres:</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="nombres" placeholder="Escriba los nombres"/>
                                </div>
                        </div>
                         <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Primer Apellido:</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="primer_apellido" placeholder="Escriba el primer apellido"/>
                                </div>
                        </div>
                         <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Segundo Apellido:</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="segundo_apellido" placeholder="Escriba el segundo apellido"/>
                                </div>
                        </div>
                        <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Contraseña:</label>
                                <div class="col-md-5">
                                    <input type="password" class="form-control" id="pass" placeholder="Escriba una contraseña"/>
                                </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info" id="btn_store_usuario">Registrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--FIN DEL MODAL-->
        <!--MODAL -->
              <div class="modal" id="modal_usuarios_update" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="defModalHead">Actualizar Usuario</h4>
                        <input type="hidden" name="" id="id_usuario">
                    </div>
                    <div class="modal-body">
                         <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Rol:</label>
                                <div class="col-md-5">
                                    <select id="cbm_rol_a" class="form-control">
                                        <option value="0">Seleccione un Rol</option>
                                        @foreach($roles as $r)
                                        <option value="{{$r->codigo_rol}}">{{$r->rol}}</option>
                                        @endforeach
                                    </select>
                                </div>
                        </div>
                        <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Nombre de Usuario:</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="nombre_usuario_a" placeholder="Escriba un nombre de usuario"/>
                                </div>
                        </div>
                         <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Nombres:</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="nombres_a" placeholder="Escriba los nombres"/>
                                </div>
                        </div>
                         <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Primer Apellido:</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="primer_apellido_a" placeholder="Escriba el primer apellido"/>
                                </div>
                        </div>
                         <div style="display: flex;padding-bottom:  10px;">
                            <label class="col-md-5 control-label">Segundo Apellido:</label>
                                <div class="col-md-5">
                                    <input type="text" class="form-control" id="segundo_apellido_a" placeholder="Escriba el segundo apellido"/>
                                </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info" id="btn_update_usuario">Actualizar</button>
                    </div>
                </div>
            </div>
        </div>
        <!--FIN DEL MODAL-->
@endsection

@section('js')
<script type="text/javascript" src="{{asset('template/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script type="text/javascript" src="{{asset('template/js/plugins.js')}}"></script>  
<script type="text/javascript" src="{{asset('js/control_usuarios.js')}}"></script>  

@endsection
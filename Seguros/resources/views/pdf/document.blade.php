<body>
    

    <table border="1" style="font-size:10px" cellspacing="1" cellpadding="3" >
        <thead>
            <tr>
                <th>CODIGO</th>
                <th>IDENTIFICACIÓN</th>
                <th>NOMBRES</th>
                <th>FECHA NACIMIENTO</th>
                <th>TELEFONO</th>
                <th>CELULAR</th>
                <th>CIUDAD</th>
                <th>DIRECCION</th>
            </tr>
        </thead>
            <tbody>
                @foreach($data as $r)
                    <tr>
                        <td>{{$r->codigo_cliente}}</td>
                        <td>{{$r->numero_identificacion}}</td>
                        <td>{{$r->nombres}}</td>
                        <td>{{$r->fecha_nacimiento}}</td>
                        <td>{{$r->telefono}}</td>
                        <td>{{$r->celular}}</td>
                        <td>{{$r->ciudad}}</td>
                        <td>{{$r->direccion}}</td>

                    </tr>
                @endforeach
            </tbody>
    </table>
</body>
<!DOCTYPE html>
<html lang="en">
    <head>        
        <!-- META SECTION -->
        <title>Laboratorio Palacio Alcivar</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('template/css/theme-default.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('plugins/sweetalertalert/sweetalert.css')}}"/>
        <link rel="stylesheet" type="text/css" href="{{asset('plugins/daterangepicker/daterangepicker-bs3.css')}}"/>
        <!--<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />-->
        <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
        @yield('css')
        <!-- EOF CSS INCLUDE -->                                    
    </head>
    <body>
        <!-- START PAGE CONTAINER -->
        @yield('content')
       

        <!-- MESSAGE BOX-->
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Salir del<strong> Sistema</strong> ?</div>
                    <div class="mb-content">
                        <p>Esta seguro de salir del sistema?</p>                    
                        <p>Presione No para continuar trabajando. Presione si para salir del sistema.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="/logout" class="btn btn-success btn-lg">Si</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->

        <!-- START PRELOADS -->
        <audio id="audio-alert" src="{{asset('template/audio/alert.mp3')}}" preload="auto"></audio>
        <audio id="audio-fail" src="{{asset('template/audio/fail.mp3')}}" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        
    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        <script type="text/javascript" src="{{asset('template/js/plugins/jquery/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('template/js/plugins/jquery/jquery-ui.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('template/js/plugins/bootstrap/bootstrap.min.js')}}"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src="{{asset('template/js/plugins/icheck/icheck.min.js')}}"></script>        
        <script type="text/javascript" src="{{asset('template/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('template/js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>
        
        <script type="text/javascript" src="{{asset('template/js/plugins/morris/raphael-min.js')}}"></script>
        <script type="text/javascript" src="{{asset('template/js/plugins/rickshaw/d3.v3.js')}}"></script>
        <script type="text/javascript" src="{{asset('template/js/plugins/rickshaw/rickshaw.min.js')}}"></script>
        <script type='text/javascript' src="{{asset('template/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script type='text/javascript' src="{{asset('template/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>                
        <script type='text/javascript' src="{{asset('template/js/plugins/bootstrap/bootstrap-datepicker.js')}}"></script>                
        <script type="text/javascript" src="{{asset('template/js/plugins/owl/owl.carousel.min.js')}}"></script>                 
        
        <script type="text/javascript" src="{{asset('template/js/plugins/moment.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('template/js/plugins/daterangepicker/daterangepicker.js')}}"></script>
        <!-- END THIS PAGE PLUGINS-->        

        <!-- START TEMPLATE -->
       
        <script type="text/javascript" src="{{asset('template/js/plugins.js')}}"></script>        
        <script type="text/javascript" src="{{asset('template/js/actions.js')}}"></script>
        <script type="text/javascript" src="{{asset('plugins/sweetalertalert/sweetalert.min.js')}}"></script>
        
        <!--<script type="text/javascript" src="{{asset('template/js/demo_dashboard.js')}}"></script>-->
        @yield('js')
        <!-- END TEMPLATE -->
    <!-- END SCRIPTS -->         
    </body>
</html>







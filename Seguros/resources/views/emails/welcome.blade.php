<!-- cabecera del mensaje -->
<table align="center" width="100%" cellspacing="0" cellpadding="0" style="background:rgb(247,247,247);" height="auto">
    <tbody>
        <tr>
            <td>
            <table border="0" cellpadding="0" cellspacing="0" width="700" style="background:rgb(247,247,247);" align="center" height="68">
                <tbody>
                    <tr height="20">
                    </tr>
                    <tr>
                        <td height="32" valign="bottom" width="150">
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
    </tbody>
</table>
<!-- cuerpo del mensaje -->
<table align="center" style="background:rgb(247,247,247);" border="0" cellpadding="0"   cellspacing="0" width="100%">
    <tbody>
        <tr>
            <td>
                <table style="background:rgb(255,255,255);border:1px solid rgb(221,221,221);" cellspacing="0" cellspacing="0" align="center" border="0" width="700">
                    <tbody>
                        <tr>
                            <td height="20"></td>
                        </tr>
                        <tr>
                            <td>
                                <table cellspacing="0" cellspacing="0" align="center" border="0" width="620">
                                    <tbody>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                        <tr>
                                            <td style="line-height:32px;font-family:arial; justify-content: center; display: flex;">
                                                <p>
                                                    <span style="font-size:24px;color: #fb6703;">
                                                        <a href="#" target="_blank">
                                                            <img align="left" src="http://178.128.6.202/img/logo_alcivar.png" alt="Logo" style="height: 120px;">
                                                        </a>
                                                    </span>
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-family:arial">
                                                <span style="color: #333;">
                                                    <span >
                                                        <br>
                                                        Estimado cliente: <strong>{{$negoci}}</strong>
                                                        <br>
                                                        <br>
                                                        <br>
                                                    </span>
                                                </span>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td>    
                                                <p style="font-family: sans-serif;line-height:20px ;text-align: justify;">Queremos darle las gracias por haber confiado en nosotros al contratar nuestro servicio de prevención de salud. <br><br> 
Para nosotros es un orgullo que se haya decidido por nuestros servicios los cuales constituyen una alianza estratégica entre varias empresas manabitas con gran trayectoria en el sector de la salud para servir a los clientes del prestigioso BANCO COMERCIAL DE MANABI. <br><br>
Nuestro servicio está disponible para atender sus necesidades por lo que esperamos estar a la altura de sus expectativas. <br><br>
Sin nada más que reiterarle nuestra bienvenida reciba un cordial saludo.
</p>
                                            </td>
                                            <tr>
                                            <td height="20"></td>
                                            </tr>
                                        </tr>
                                       
                                        
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                       
                                        <tr>
                                            <td><span style="font-weight:bold">
                                                Dr. Pablo Palacio Alcívar   <br>
GERENTE LAB CENTRO PALACIO CIA LTDA <br><br><br>

                                            </span></td>
                                        </tr>
                                        <tr>
                                            <td height="20"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>

<!-- pie del mensaje -->
<table align="center" width="100%" cellspacing="0" cellpadding="0" style="background:rgb(247,247,247);" height="auto">
    <tbody>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="auto" style="background:rgb(247,247,247);" align="center" height="68">
                    <tbody>
                        <tr height="20"></tr>
                            <tr>
                                <td height="32" valign="bottom" width="150">
                                
                                </td>
                            </tr>
                        <tr height="20"></tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>
        
@extends('layout.app')

@section('css')
    <link rel="stylesheet" href="{{asset('plugins/datapicker/css/bootstrap-datetimepicker.min.css')}}">
@endsection

@section('content')
     <div class="page-container">
            
            <!-- START PAGE SIDEBAR -->
            <div class="page-sidebar">
                <!-- START X-NAVIGATION -->
                <ul class="x-navigation">
                    <li class="xn-logo">
                    <a href="/">LAB-Palacio Alcivar</a>
                        <a href="#" class="x-navigation-control"></a>
                    </li>
                    <li class="xn-profile">
                        <a href="#" class="profile-mini">
                            <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                        </a>
                        <div class="profile">
                            <div class="profile-image">
                                <img src="{{asset('template/assets/images/users/avatar.jpg')}}" alt="John Doe"/>
                            </div>
                            <div class="profile-data">
                                <div class="profile-data-name">{{Session::get('usuario')}}</div>
                                <div class="profile-data-title">{{Session::get('nombres_apellidos')}}</div>
                            </div>

                        </div>                                                                        
                    </li>
                   <li class="xn-title">Navigation</li>
                   @if(Session::get('rol')==1)
                    <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-user"></span> <span class="xn-text">Usuarios</span></a>
                        <ul>
                            <li><a href="/usuarios">Administrar Usuario</a></li>
                            <li><a href="/roles">Administrar Roles</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <!--<li><a href="/ventas/reporte">Reporte Ventas</a></li>-->
                        </ul>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                    @elseif(Session::get('rol')==2)
                     <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Clientes</span></a>
                        <ul>
                            <li><a href="/clientes">Administrar Clientes</a></li>

                        </ul>
                    </li>
                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Seguros</span></a>
                        <ul>
                            <li><a href="/seguros">Administrar Seguros</a></li>
                        </ul>
                    </li>
                    <li class="xn-openable active">
                        <a href="#"><span class="fa fa-file-text-o"></span> <span class="xn-text">Visitas</span></a>
                        <ul>
                            <li><a href="/administrar-visitas">Administrar Visitas</a></li>
                        </ul>
                    </li>
                     @elseif(Session::get('rol')==13)
                     <li >
                        <a href="/home"><span class="fa fa-home"></span> <span class="xn-text">Inicio</span></a>
                    </li>

                   
                    <li class="xn-openable">
                        <a href="#"><span class="fa fa-users"></span> <span class="xn-text">Ventas</span></a>
                        <ul>
                            <li><a href="/ventas">Nueva Ventas</a></li>
                            <li><a href="/ventas/admin">Administrar Ventas</a></li>
                            <li><a href="/ventas/reporte">Reporte Ventas</a></li>
                        </ul>
                    </li>
                    @endif

                    
                </ul>
                <!-- END X-NAVIGATION -->
            </div>
            <!-- END PAGE SIDEBAR -->
            
            <!-- PAGE CONTENT -->
            <div class="page-content">
                
                <!-- START X-NAVIGATION VERTICAL -->
                <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
                    <!-- TOGGLE NAVIGATION -->
                    <li class="xn-icon-button">
                        <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
                    </li>
                    <!-- END TOGGLE NAVIGATION -->
                    <!-- SEARCH -->
                    <li class="xn-search">
                        <form role="form">
                            <input type="text" name="search" placeholder="Search..."/>
                        </form>
                    </li>   
                    <!-- END SEARCH -->
                    <!-- SIGN OUT -->
                    <li class="xn-icon-button pull-right">
                        <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>                        
                    </li> 
                    <!-- END SIGN OUT -->
  
                </ul>
                <!-- END X-NAVIGATION VERTICAL -->                     
                <!-- START BREADCRUMB -->
                <ul class="breadcrumb">
                    <li><a href="#">Inicio</a></li>                    
                    <li><a href="#">Visitas</a></li>
                    <li class="active">Administrar Visitas</li>
                </ul>
                <!-- END BREADCRUMB -->

                <!-- PAGE TITLE -->
                <div class="page-title">                    
                    <h2><span class="fa fa-arrow-circle-o-left"></span> Administración de Visitas</h2>
                </div>
                <!-- END PAGE CONTENT WRAPPER -->    

                <div class="page-content-wrap">                
                
                    <div class="row">
                        <div class="col-md-12">
                            <!-- START DEFAULT DATATABLE -->
                            <div class="panel panel-default">
                                <div class="panel-heading">                                
                                    <div class="col-12 col-sm-6 ">
                                        <div class="input-group">
                                            <span class="input-group-addon">Seleccionar cliente:</span>
                                            <input type="text" id="nombres" class="form-control" placeholder="cédula" aria-describedby="basic-addon2">
                                            <span class="input-group-addon" id="basic-addon2"><i class="fa fa-search"></i></span>
                                            
                                        </div>
                                        <div class="col-12" style="display:none;  height:150px; overflow-x: scroll;" id="div-search-hd"></div> 
                                    </div> 
                                </div>
                                <div class="panel-body" id="div-result">
                          
                                </div>
                            </div>
                            <!-- END DEFAULT DATATABLE -->
                        </div>
                    </div>                            
                </div>
            </div>            
            <!-- END PAGE CONTENT -->
        </div>
        <!-- END PAGE CONTAINER -->

        <div class="modal fade" id="modal_visitas" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Agregar Visita</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12">
                        <table class="table">

                            <tr>
                                <td>Uso del seguro:</td>
                                <td id="div-seguro">
                                    
                                </td>
                            </tr>
                            <tr>
                                <td>Tipo de Servicio:</td>
                                <td>
                                    <select id="select-seguro" class="form-control">
                                        <option value="1">Exámenes Médicos
                                        <option value="2">Atención Odontológica
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"><button class="btn btn-success pull-right" id="btn-save-visita">Guardar Visita</button></td>
                            </tr>
                        </table>
                    </div>
                </div>   
            </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="modal_visitas_ver" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="gridSystemModalLabel">Historial de visitas</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-12" id="tbl-visitas">

                    </div>
                </div>   
            </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
@endsection

@section('js')
<script type="text/javascript" src="{{asset('template/js/plugins/datatables/jquery.dataTables.min.js')}}"></script>    
<script type="text/javascript" src="{{asset('template/js/plugins.js')}}"></script>  
<script src="{{asset('plugins/fullcalendar/moment.min.js')}}"></script>
<script src="{{asset('plugins/datapicker/js/bootstrap-datetimepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('js/control_adminvisitas.js')}}"></script>


@endsection
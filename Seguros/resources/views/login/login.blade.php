<!DOCTYPE html>
<html lang="en" class="body-full-height">
    <head>        
        <!-- META SECTION -->
        <title>Laboratorio Palacio Alcivar</title>            
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        
        <link rel="icon" href="favicon.ico" type="image/x-icon" />
        <!-- END META SECTION -->
        
        <!-- CSS INCLUDE -->        
        <link rel="stylesheet" type="text/css" id="theme" href="{{asset('template/css/theme-default.css')}}"/>
        <!-- EOF CSS INCLUDE -->   
        <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
                                          
    </head>
    <body>
        
        <div class="login-container lightmode">
        
            <div class="login-box animated fadeInDown">
        <!--<input type="date" max="2018-08-15"  value="2018-01-01" class="form-control">-->
               
                <img src="/img/logo_alcivar.png" class="img-responsive center-block" style="max-width:300px; margin-bottom:30px" alt="Laboratorio Alcivar<" />
                <div class="login-body">
                    <div class="login-title"><strong>Ingrese</strong> al sistema</div>
                    <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" class="form-control" id="usuario" placeholder="Usuario"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" class="form-control" id="pass" placeholder="Password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6">
                            <a href="#" class="btn btn-link btn-block">Recuperar contraseña?</a>
                        </div>
                        <div class="col-md-6">
                            <button class="btn btn-info btn-block" id="btn_iniciar">Iniciar Sessión</button>
                        </div>
                    </div>
                
                </div>
                </div>
                <div class="login-footer">
                    <div class="pull-left">
                        &copy; 2018 Laboratorio Palacio Alcivar
                    </div>
                    <!--<div class="pull-right">
                        <a href="#">About</a> |
                        <a href="#">Privacy</a> |
                        <a href="#">Contact Us</a>
                    </div>-->
                </div>
            </div>
            
        </div>
        <script type="text/javascript" src="{{asset('template/js/plugins/jquery/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/control_login.js')}}"></script>
        
    </body>
</html>







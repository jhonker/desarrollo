<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get("error",function(){
    abort(404);
});


Route::get('email','VentasController@email');
/*RUTAS DE LOGIN*/

Route::get('/','LoginController@index');

Route::post('login','LoginController@store');

Route::get('logout','LoginController@logout');


/*RUTAS DE HOME*/
Route::get('home','HomeController@index');
Route::get('get_count_clientes','HomeController@get_count');
Route::get('get_count_totales_nuevos','HomeController@get_count_total_nuevos');
Route::get('get_count_totales_cobrados_mensual','HomeController@get_count_total_cobra_mensual');
Route::get('get_count_totales_cancelados','HomeController@get_count_total_cancelados');
Route::get('get_count_totales_activos','HomeController@get_count_total_activos');
Route::get('get_periodo','HomeController@get_count_periodo');


Route::get('get_top_ventas','HomeController@get_top_ventas');
Route::get('get_top_ventas1','HomeController@get_top_ventas1');

/*RUTAS DE USUARIOS*/

Route::get('usuarios','UsuarioController@index');
Route::post('usuarios','UsuarioController@store');
Route::get('get_usuarios','UsuarioController@get_usuarios');
Route::get('usuario/edit/{id}','UsuarioController@edit');
Route::post('update_usuario','UsuarioController@update');
Route::post('delete_usuario','UsuarioController@delete');

/*RUTAS DE ROLES*/
Route::get('roles','RolesController@index');
Route::get('roles/{id}','RolesController@edit');
Route::post('roles','RolesController@store');
Route::post('delete_rol','RolesController@delete');
Route::get('get_roles','RolesController@get_roles');
Route::post('update_rol','RolesController@update');

/*RUTAS DE CLIENTES*/
Route::get('clientes','ClientController@index');

    Route::get('clientes/downloadExcelClient/{type}', 'MaatwebsiteDemoController@downloadExcelClient');
    Route::get('clientes/downloadPdfClient', 'PdfController@generatePdfCliente');
    Route::post('clientes/importarExcelClientes', 'MaatwebsiteDemoController@importExcelClient');
    Route::post('clientes/eliminar-clientes','ClientController@delete');
    Route::post('clientes/agregar-cliente','ClientController@agregarCliente');
    Route::get('clientes/listado-cliente','ClientController@get_clientes');
    Route::get('clientes/search/{id}','ClientController@get_cliente_id');
    Route::post('clientes/modificar-cliente','ClientController@modificarCliente');
    Route::post('clientes/listado-cliente-load','ClientController@listClientLoad');
    Route::post('clientes/listado-cliente-search','ClientController@get_clientes_search');
    Route::post('api/clientes',function(){
        return DataTables::query(DB::table('lb_clientes')->select("codigo_cliente","numero_identificacion","nombres","fecha_nacimiento","telefono","celular","ciudad","direccion"))->toJson();
    });  
    
/*RUTAS VENTAS*/
Route::get('ventas','VentasController@index');
Route::get('ventas/new','VentasController@new');
Route::get('select_cliente/{id}','VentasController@seleccionar');
Route::get('get_cuentas/{id}','VentasController@get_cuentas');
Route::get('get_tipo_cuenta/{id}','VentasController@get_tipo_cuenta');
Route::get('get_resul_seguro/{id}','VentasController@get_resul_seguro');
Route::post('store_venta','VentasController@store');
Route::get('ventas/admin','VentasAdminController@ventasAdmin');
Route::post('ventas/admin/importar-captaciones', 'MaatwebsiteDemoController@importarCaptaciones');
Route::post('ventas/admin/importar-cartera-captaciones', 'MaatwebsiteDemoController@importarCarteraCaptaciones');
Route::post('ventas/admin/importar-cancelaciones', 'MaatwebsiteDemoController@importarCancelaciones');
Route::get('ventas/reporte', 'ReporteVentaControllers@index');
Route::get('get_ventas_todos/{fecha1}/{fecha2}','ReporteVentaControllers@get_ventas_todos');
Route::get('get_ventas_usuario/{usuario}/{fecha1}/{fecha2}','ReporteVentaControllers@get_ventas_usuario');

Route::get('ventas/reporte/downloadExcelVenta/{type}', 'MaatwebsiteDemoController@downloadExcelVenta');
Route::get('ventas/reporte/downloadPdfVenta', 'PdfController@generatePdfVenta');
Route::post('update_datos_cliente','VentasController@update_datos_cliente');
Route::get('ventas/update/{id}','VentasController@updateVenta');
Route::post('update_venta','VentasController@update_venta');
Route::get('get_dependientes/{cedula}','VentasController@GetDependientes');
Route::post('AddDependientes','VentasController@AddDependientes');
Route::post('delete_dependiente','VentasController@delete_dependiente');
/*SEGUROS*/

Route::get('seguros','SeguroController@index');
Route::post('seguros','SeguroController@store');
Route::get('seguros/{id}','SeguroController@edit');
Route::get('get_seguros','SeguroController@get_seguros');
Route::post('seguro_update','SeguroController@update');
Route::post('seguros_delete','SeguroController@delete_s');

/*ADMINISTRACIÓN DE VISITAS*/
Route::get('/administrar-visitas','AdminVisitasControllers@index');
Route::post('/clientes/seguros','AdminVisitasControllers@get_clientes_seguros');
Route::post('/clientes/dependientes','AdminVisitasControllers@get_clientes_dependienttes');
Route::post('/administrar-visitas/agregar','AdminVisitasControllers@agregarVisita');
Route::post('/administrar-visitas/toma','AdminVisitasControllers@tomaVisita');
<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\negocio;

class Welcome extends Mailable
{
    use Queueable, SerializesModels;


    public $negoci;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($negoci)
    {
        $this->negoci = $negoci;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return storage_path('app/public');
        return $this->view('emails.welcome')
        ->from('infoserviciosmedicosbcm@gmail.com','Servicios de Salud BCM')
        ->attach(public_path('dist')."/CondicionesdeServicios.pdf")
        ->subject('CONTRATO DE SERVICIO DE SALUD CLIENTES DEL BANCO COMERCIAL DE MANABI');
    }
}

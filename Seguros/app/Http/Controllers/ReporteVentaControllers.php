<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use DB;

use Session;


class ReporteVentaControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $date = Carbon::now();

       // $date = $date->format('Y-m-d');
        $ventas2 = DB::select("select usuario_adicion,sum(tipo_producto) from view_lb_reportes_ventas where fecha_adicion::Date BETWEEN ?::Date AND ?::Date group by usuario_adicion",[$date,$date]);
        //$ventas2 = DB::select("select usuario_adicion,sum(tipo_producto) from view_lb_reportes_ventas where fecha_adicion::Date BETWEEN '2018-09-27'::Date AND '2018-09-27'::Date group by usuario_adicion ");
       // return $ventas2;
        $usuario =DB::table("view_lb_usuarios_del_sistema")->get();
        //$ventas = DB::table("view_lb_reportes_ventas")->whereDay('fecha_adicion', $date)->get();
        //return $usuario;
        return view('Ventas.reporteVenta',compact('usuario','ventas2'));
    }

    public function get_ventas_todos($fecha1,$fecha2){
         $ventas = DB::select("select usuario_adicion,sum(tipo_producto) from view_lb_reportes_ventas where fecha_adicion::Date BETWEEN '$fecha1'::Date AND '$fecha2'::Date group by usuario_adicion ");

         return $ventas;
    }
    public function get_ventas_usuario($usuario, $f,$ff){
        $ventas  =DB::select("select * from view_lb_reportes_ventas where usuario_adicion ='$usuario' and fecha_adicion::Date BETWEEN '$f'::Date AND '$ff'::Date ");
        return $ventas;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */




    

    public function get_clientes(){
        $roles =DB::table('view_lb_clientes')->get();
        return $roles;
    }

    


    


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

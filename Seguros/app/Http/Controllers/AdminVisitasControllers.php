<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use DB;

use Session;

class AdminVisitasControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Visitas.adminVisitas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    
    public function get_cliente_id($id)
    {
        $rol = DB::table("view_lb_clientes")->where('codigo_cliente', '=', $id)->get();
      
        $resul;
        if($rol!='[]'){

            $resul=true;
            //return response()->json(["RES"=>true,"rol"=>$rol_]);
        }else{
            $resul=false;
        }
        return response()->json(["RES"=>$resul,"cliente"=>$rol]);
    }

    public function delete(Request $r){
      $rol = DB::select('select proc_eliminar_cliente(?)',[$r->id]);  

      if($rol){
            return response()->json(["RES"=>$rol]);
      }else{
            return response()->json(["RES"=>$rol]);
      }
    }

    
    public function get_clientes_seguros(request $r)
    {
        $rol = DB::table("view_lb_seguros")->where('codigo_cliente', '=', $r->id)->get();
      
        $resul;
        if($rol!='[]'){

            $resul=true;
            //return response()->json(["RES"=>true,"rol"=>$rol_]);
        }else{
            $resul=false;
        }
        return response()->json(["RES"=>$resul,"cliente"=>$rol]);
    }

    public function get_clientes(){
        $roles =DB::table('view_lb_clientes')->get();
        return $roles;
    }

    

    public function agregarCliente(Request $r){

        $insert[] = [
                        'numero_identificacion' => $r->codigo, 
                        'nombres' =>  strtoupper ($r->names),
                        'fecha_nacimiento' => $r->date,
                        'telefono' => $r->telefono,
                        'celular' => $r->celular,
                        'correo_electronico' => $r->correo,
                        'direccion' => strtoupper($r->direccion),
                        'ciudad' => strtoupper($r->ciudad),
                        'provincia' => strtoupper($r->provincia),
                        'genero' => strtoupper($r->genero),
                        'codigo_cuenta' => $r->cuenta,
                        'tipo_cuenta' => strtoupper($r->tipo)


                    ];
        $sqlInsert = json_encode($insert);
        //return response()->json(["RES"=>$insert]);
        $roles =DB::select('select proc_registrar_clientes(?,?,?)',[$sqlInsert,date("Y-m-d"),Session::get('usuario')]);
        if($roles){
            return response()->json(["RES"=>$roles]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    
    public function agregarVisita(Request $r){
        $arr1 = $r->arraayDependiente;

        $nn = count($arr1);
       
        for($i =0; $i<$nn; $i++){
   
            $insert[$i] = [
                'codigo_cliente' => $r->codigo_cliente, 
                'tipo' => $r->tipo, 
                'codigo_dependiente' => $arr1[$i]
            ];

        }
        
        $sqlInsert = json_encode($insert);
       
        $roles =DB::select('select proc_registrar_visitas(?,?,?)',[$sqlInsert,date("Y-m-d"),Session::get('usuario')]);
        if($roles){
            return response()->json(["RES"=>$roles]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    public function get_clientes_dependienttes(Request $r){

        $rol = DB::table("view_lb_clientes_dependientes")->where('numero_identificacion_titular', '=', $r->id)->get();
      
        $resul;
        if($rol!='[]'){

            $resul=true;
            //return response()->json(["RES"=>true,"rol"=>$rol_]);
        }else{
            $resul=false;
        }
        return response()->json(["RES"=>$resul,"dependientes"=>$rol]);
    }

    public function tomaVisita(request $r)
    {
        $rol = DB::table("view_lb_visitas")->where('numero_identificacion_titular', '=', $r->id)->get();
      
        $resul;
        if($rol!='[]'){

            $resul=true;
            //return response()->json(["RES"=>true,"rol"=>$rol_]);
        }else{
            $resul=false;
        }
        return response()->json(["RES"=>$resul,"dependientes"=>$rol]);
    }
    
    //

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

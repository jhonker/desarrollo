<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use DB;

use Session;

use Redirect;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('usuario')){
            $client =DB::table("lb_clientes")->select("codigo_cliente","numero_identificacion","nombres","fecha_nacimiento","telefono","celular","ciudad","direccion")->take(10)->get();
            return view('Client.client',compact('client'));
        }else{
            return Redirect::to('/');
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $date = Carbon::now();
     
        $rol= DB::table('lb_roles')->insert([
            'rol' => $request->rol, 
            'fecha_adicion' => $date,
            'fecha_modificacion' => $date,
            'modificado_por' => "DEMON"
        ]);   

        if($rol){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    
    public function get_cliente_id($id)
    {
        $rol = DB::table("view_lb_clientes")->where('codigo_cliente', '=', $id)->orderBy('codigo_cliente', 'ASC')->take(100)->get();
      
        $resul;
        if($rol!='[]'){

            $resul=true;
            //return response()->json(["RES"=>true,"rol"=>$rol_]);
        }else{
            $resul=false;
        }
        return response()->json(["RES"=>$resul,"cliente"=>$rol]);
    }

    public function delete(Request $r){
      $rol = DB::select('select proc_eliminar_cliente(?)',[$r->id]);  

      if($rol){
            return response()->json(["RES"=>$rol]);
      }else{
            return response()->json(["RES"=>$rol]);
      }
    }

    

    public function get_clientes(){
        $roles =DB::table('view_lb_clientes')->orderBy('codigo_cliente', 'ASC')->take(100)->get();
        return $roles;
    }

    public function get_clientes_search(Request $r){
        $roles =DB::table('view_lb_clientes')->where('numero_identificacion', 'LIKE', '%'.trim($r->search).'%')->orWhere('nombres', 'LIKE', '%'.trim($r->search).'%')->orderBy('nombres', 'ASC')->take(10)->get();
        return $roles;
    }

    public function listClientLoad(Request $r){
        $roles =DB::table('view_lb_clientes')->where('codigo_cliente', '>', $r->limits)->orderBy('codigo_cliente', 'ASC')->take(100)->get();
        return $roles;
    }
    
    public function agregarCliente(Request $r){

        $insert[] = [
                        'numero_identificacion' => $r->codigo, 
                        'nombres' =>  strtoupper ($r->names),
                        'fecha_nacimiento' => $r->date,
                        'telefono' => $r->telefono,
                        'celular' => $r->celular,
                        'correo_electronico' => $r->correo,
                        'direccion' => strtoupper($r->direccion),
                        'ciudad' => strtoupper($r->ciudad),
                        'provincia' => strtoupper($r->provincia),
                        'genero' => strtoupper($r->genero),
                        'codigo_cuenta' => $r->cuenta,
                        'tipo_cuenta' => strtoupper($r->tipo)


                    ];
        $sqlInsert = json_encode($insert);
        //return response()->json(["RES"=>$insert]);
        $roles =DB::select('select proc_registrar_clientes(?,?,?)',[$sqlInsert,date("Y-m-d"),Session::get('usuario')]);
        if($roles){
            return response()->json(["RES"=>$roles]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }
    
    public function modificarCliente(Request $r){

        $insert[] = [
                        'numero_identificacion' => $r->codigo, 
                        'nombres' => strtoupper($r->names),
                        'fecha_nacimiento' => $r->date,
                        'telefono' => $r->telefono,
                        'celular' => $r->celular,
                        'correo_electronico' => $r->correo,
                        'direccion' => strtoupper($r->direccion),
                        'ciudad' => strtoupper($r->ciudad),
                        'provincia' => strtoupper($r->provincia),
                        'genero' => strtoupper($r->genero),
                        'codigo_cliente' => $r->id



                    ];
        $sqlInsert = json_encode($insert);
        //return response()->json(["RES"=>$insert]);
        $roles =DB::select('select proc_actualizar_cliente(?,?,?)',[$sqlInsert,date("Y-m-d"),Session::get('usuario')]);
        if($roles){
            return response()->json(["RES"=>$roles]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;

use Redirect;

use DB;


class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if (Session::get('usuario')){
              /* Session()->put('usuario', "ADMIN");
                Session()->put('nombres_apellidos', "JHONY GUAMAN");
                Session()->put('rol', "ADMIN");*/
            return view('home.home');
        }else{
            return Redirect::to('/');
        }
    }

    public function get_top_ventas1(){
        $top=DB::select("select * from view_lb_ventas order by fecha_adicion ASC");
        return $top;
    }

    public function get_top_ventas(){
        $top1 = DB::select("select * from view_lb_ventas order by fecha_adicion ASC");
        return $top1;
    }
    public function get_count(){
      if (Session::get('usuario')){
          $client =DB::select("select count(codigo_cliente) as c from lb_clientes");
          return $client;
       }else{
           return Redirect::to('/');
       }
    }

    public function get_count_total_nuevos(){
        if (Session::get('usuario')){
            $client =DB::table("view_lb_totales_nuevos")->get();
            return $client;
         }else{
             return Redirect::to('/');
         }
    }

    public function get_count_total_cobra_mensual(){
        if (Session::get('usuario')){
            $client =DB::table("view_lb_totales_cobrados_mensual")->get();
            return $client;
         }else{
             return Redirect::to('/');
         }
    }

    public function get_count_total_cancelados(){
        if (Session::get('usuario')){
            $client =DB::table("view_lb_totales_cancelados")->get();
            return $client;
         }else{
             return Redirect::to('/');
         }
    }

    public function get_count_total_activos(){
        if (Session::get('usuario')){
            $client =DB::table("view_lb_totales_clientes")->get();
            return $client;
         }else{
             return Redirect::to('/');
         }
    }

    public function get_count_periodo(){
        if (Session::get('usuario')){
            $client =DB::table("view_lb_periodo")->get();
            return $client;
         }else{
             return Redirect::to('/');
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

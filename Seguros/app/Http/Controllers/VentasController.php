<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Carbon\Carbon;

use Session;

use Illuminate\Support\Facades\Mail;

use \App\Mail\Welcome;


class VentasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('usuario')){
            $ventas = DB::table('lb_clientes_x_seguro')->get();
            return view('Ventas.venta',compact('ventas'));
        }else{
            return Redirect::to('/');
        }
        
       
    }

    public function email(){
        $email= Mail::to("pjpalacio@hotmail.com","Palacio Alcívar")->send(new Welcome("Palacio Alcívar"));

    }

    public function updateVenta($id){
        $cliente = DB::select("select c.codigo_cliente, c.numero_identificacion, concat(c.nombres ,c.primer_apellido,c.segundo_apellido)as cliente, c.telefono, c.correo_electronico, c.celular, cs.codigo_cuenta 
            from lb_clientes c, lb_clientes_x_seguro cs where c.numero_identificacion = '$id' and cs.numero_identificacion =c.numero_identificacion ");
        $seguros = DB::table('lb_seguros')->where("estado","=","A")->get();

        $dependientes = DB::table('lb_clientes_dependientes')->select('codigo_cliente_dependiente','numero_identificacion','nombres','fecha_nacimiento','genero','parentezco')->where("numero_identificacion_titular","=",$id)->get();

        
        return view('Ventas.update_venta',compact('cliente','seguros','dependientes'));
    }

    public function GetDependientes($cedula){
         $dependientes = DB::table('lb_clientes_dependientes')->select('codigo_cliente_dependiente','numero_identificacion','nombres','fecha_nacimiento','genero','parentezco')->where("numero_identificacion_titular","=",$cedula)->get();
         return $dependientes;
    }

    public function delete_dependiente(Request $r){
        DB::table('lb_clientes_dependientes')->where('codigo_cliente_dependiente', '=', $r->id)->delete();
        return response()->json(["RES"=>true]);

    }
    public function new(){
        if (Session::get('usuario')){
            $seguros = DB::table('lb_seguros')->where("estado","=","A")->get();
            return view("Ventas.new_venta",compact('seguros'));
        }else{
            return Redirect::to('/');
        }
        
    }

    public function AddDependientes(Request $r){
        $date = Carbon::now();
        if (Session::get('usuario')){
            $dependientes = DB::table('lb_clientes_dependientes')->insert(['numero_identificacion' => $r->identificacion,
                             'numero_identificacion_titular' => $r->titular,
                             'nombres'=>$r->nombres,
                             'fecha_nacimiento'=>$r->f_nacimiento,
                             'parentezco'=>$r->parentesco,
                             'genero'=>$r->genero,
                             'situacion' =>'A',
                             'usuario_adicion'=>Session::get('usuario'),
                             'fecha_ingreso'=>$date,
                            ]);
            return response()->json(["RES"=>true]);
        }else{
            return Redirect::to('/');
        }
    }
    public function update_datos_cliente(Request $r){
         $date = Carbon::now();

         if (Session::get('usuario')){
            $update =  DB::select("select proc_actualiza_datos(?,?,?,?,?,?)",[intval($r->codigo),$r->telefono,$r->celular,$r->correo,$date,Session::get('usuario')]); 
            $update_="";
            foreach ($update as $r) {
                $update_=$r->proc_actualiza_datos;
            }
            if($update_=="OK"){
                return response()->json(["RES"=>true]);
            }else{
                return response()->json(["RES"=>false,"RESP"=>$update_]);
            }
        }else{
            return Redirect::to('/');
        }
    }

    public function get_resul_seguro($id){
        $seguro =DB::table('lb_seguros')->where('codigo_seguro','=',$id)->get();
        return $seguro;
    }
    public function seleccionar($id){
        $seleccion = DB::table('lb_clientes')->where("codigo_cliente","=",$id)->get();
        $id="";$identificacion ="";$telefono="";$nombres="";$correo="";$celular="";
        foreach ($seleccion as $s) {
            $id=$s->codigo_cliente;
            $identificacion=$s->numero_identificacion;
            $telefono =$s->telefono;
            $celular = $s->celular;
            $nombres=$s->nombres;
            $correo=$s->correo_electronico;
        }        

        return response()->json(["RES"=>true,"id"=>$id,"identificacion"=>$identificacion,"telefono"=>$telefono,"nombres"=>$nombres,"correo"=>$correo,"celular"=>$celular]);

    }

    public function get_cuentas($id){
        $cuentas = DB::table('lb_cuentas_x_cliente')->where("numero_identificacion","=",$id)->get();
        return $cuentas;
    }

    public function get_tipo_cuenta($id){
        $cuentas = DB::table('lb_cuentas_x_cliente')->where("codigo_cuenta","=",$id)->get();
        return $cuentas;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $date = Carbon::now();
        $venta =DB::select("select proc_registrar_venta(?,?,?,?,?,?)",[$request->n_identificacion,$request->nombres,$request->codigo_cuenta,$date,Session::get('usuario'),$request->depenencia]);
        $venta_="";
        foreach ($venta as $r) {
            $venta_=$r->proc_registrar_venta;
        }
        if($venta_=="OK"){
            if($request->correo!=""){
                $email= Mail::to($request->correo,$request->nombres)->send(new Welcome($request->nombres));
            }
            
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false,"RESP"=>$venta]);
        }
    }

    public function update_venta(Request $r){
        $date = Carbon::now();
        $venta =DB::select("select proc_modificar_venta(?,?,?,?)",[$r->in_numero_identificacion,$r->in_codigo_cuenta,$date,Session::get('usuario')]);
        $venta_="";
        foreach ($venta as $r) {
            $venta_=$r->proc_modificar_venta;
        }
        if($venta_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false,"RESP"=>$venta]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

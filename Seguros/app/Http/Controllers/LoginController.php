<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use Hash;

use Session;

use Redirect;
class LoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('usuario')){
            return Redirect::to('home');
        }else{
            return view("login.login");
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //return response()->json(["sms"=>"login" ]);
        $usuario = DB::select("select proc_consulta_usuario(?)",[$request->usuario]);
        
        $hash="";

        foreach($usuario as $u){
            $hash=$u->proc_consulta_usuario;
        }

        if(Hash::check($request->pass,$hash)){
            $user = DB::table("lb_usuarios_del_sistema")->where("usuario","=",$request->usuario)->get();
            foreach ($user as $us) {
                $request->session()->put('usuario', $us->usuario);
                $request->session()->put('nombres_apellidos', $us->nombres.' '.$us->primer_apellido.' '.$us->segundo_apellido);
                $request->session()->put('rol', $us->codigo_rol);
            }
            return response()->json(["sms"=>true]);
            
        }else{
            return response()->json(["sms"=>false]);
        }
    }

    public function logout(){ 
        Session::flush();
        return Redirect::to('');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

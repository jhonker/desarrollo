<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Carbon\Carbon;

use DB;

use Session;

use Redirect;


class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('usuario')){
            $roles =DB::table("lb_roles")->get();
            return view('Roles.roles',compact('roles'));
        }else{
            return Redirect::to('/');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $date = Carbon::now();
     
        /*$rol= DB::table('lb_roles')->insert([
            'rol' => $request->rol, 
            'fecha_adicion' => $date,
            'fecha_modificacion' => $date,
            'modificado_por' => "DEMON"
        ]);   */

        $rol =DB::select("select proc_registrar_rol(?,?,?)",[$request->rol,$date,Session::get('usuario')]);
        $rol_="";
        foreach ($rol as $r) {
            $rol_=$r->proc_registrar_rol;
        }
        if($rol_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = DB::table("lb_roles")->where('codigo_rol', '=', $id)->get();
        $rol_="";
        $resul;
        if($rol!='[]'){
            foreach($rol as $r){
                $rol_=$r->rol;

            }
            $resul=true;
            //return response()->json(["RES"=>true,"rol"=>$rol_]);
        }else{
            $resul=false;
        }
            return response()->json(["RES"=>$resul,"rol"=>$rol_]);
    }

    public function delete(Request $r){
      $rol = DB::select("select proc_eliminar_rol(?)",[$r->id]);
      $rol_="";
        foreach ($rol as $r) {
            $rol_=$r->proc_eliminar_rol;
        }
        if($rol_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    public function get_roles(){
        $roles =DB::table('lb_roles')->get();
        return $roles;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r)
    {
        $date = Carbon::now();
        $rol = DB::select("select proc_actualizar_rol(?,?,?,?)",[$r->id,$r->rol,$date,Session::get('usuario')]);
        $rol_="";
        foreach ($rol as $r) {
            $rol_=$r->proc_actualizar_rol;
        }
        if($rol_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

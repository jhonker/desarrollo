<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Session;

use Redirect;

use DB;

use Carbon\Carbon;

class SeguroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('usuario')){
            $seguros =DB::table("lb_seguros")->where("estado","=","A")->get();
            return view('Seguros.seguros',compact('seguros'));
        }else{
            return Redirect::to('/');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $date = Carbon::now();

         $seguro =DB::select("select proc_registrar_seguro(?,?,?,?,?,?,?,?)",[$request->seguro,$request->valor,$request->mes_sig,$request->mes_cobro,$request->mes_anterior,$request->max_atraso,$date,Session::get('usuario')]);

          $seguro_="";
        foreach ($seguro as $r) {
            $seguro_=$r->proc_registrar_seguro;
        }
        if($seguro_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false,"R"=>$seguro_]);
        }

    }
    public function get_seguros(){
        $seguros =DB::table("lb_seguros")->where("estado","=","A")->get();
        return $seguros;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $seguros =DB::table("lb_seguros")->where("codigo_seguro","=",$id)->get();
        $descripcion = ""; $valor = ""; $mes_anio_siguiente=""; $mes_anio_ant=""; $mes_anio_cobrar = ""; $resul;
        if($seguros!='[]'){
            foreach($seguros as $s){
                $descripcion =$s->descripcion; $valor = $s->valor; $mes_anio_siguiente = $s->mes_anio_siguiente; $mes_anio_cobrar = $s->mes_anio_cobrar; $mes_anio_ant=$s->mes_anio_anterior; 
            }
            $resul=true;
        }else{
            $resul=false;
        }
            return response()->json([
                "RES"=>$resul,
                "descripcion"=>$descripcion,
                "valor" =>$valor,
                "mes_anio_cobrar" =>$mes_anio_cobrar,
                "mes_anio_siguiente" => $mes_anio_siguiente,
                "mes_anio_ant"=>$mes_anio_ant]);
    }

    public function delete_s(Request $r){
        $date = Carbon::now();
        $seguro = DB::select("select proc_eliminar_seguro(?,?,?)",[$r->id,$date,Session::get('usuario')]);
        $seguro_="";
        foreach ($seguro as $r) {
            $seguro_=$r->proc_eliminar_seguro;
        }
        if($seguro_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $date = Carbon::now();
        $seguro = DB::select("select proc_actualizar_seguro(?,?,?,?,?,?)",[$request->id,$request->mes_sig,$request->mes_cobro,$request->mes_ant,$date,Session::get('usuario')]);
        $seguro_="";
        foreach ($seguro as $r) {
            $seguro_=$r->proc_actualizar_seguro;
        }
        if($seguro_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

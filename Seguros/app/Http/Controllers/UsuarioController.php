<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use Carbon\Carbon;

use Session;

use Redirect;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Session::get('usuario')){
            $user =DB::table("lb_usuarios_del_sistema")->get();
            $roles =DB::table("lb_roles")->get();
            return view('usuarios.usuario',compact('roles','user'));
        }else{
            return Redirect::to('/');
        }
       
    }

    public function get_usuarios(){
         $usuarios =DB::table('lb_usuarios_del_sistema')->get();
        return $usuarios;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $date = Carbon::now();
     
           

        $usuario = DB::select("select proc_registrar_usuario(?,?,?,?,?,?,?,?)",[$request->nombre_usuario,$date,$request->nombres,$request->p_apellido,$request->s_apellido,$request->rol,bcrypt($request->pass),Session::get('usuario')]);
       $usuario_="";
        foreach ($usuario as $r) {
            $usuario_=$r->proc_registrar_usuario;
        }
        if($usuario_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuarios = DB::table("lb_usuarios_del_sistema")->where('codigo_usuario', '=', $id)->get();
         $id="";
         $usuario = "";
         $nombres = "";
         $p_apellido="";
         $s_apellido="";
         $rol = "";

        $resul;
        if($usuarios!='[]'){
            foreach($usuarios as $r){
                $id=$r->codigo_usuario;
                $usuario =$r->usuario;
                $nombres = $r->nombres;
                $p_apellido = $r->primer_apellido;
                $s_apellido = $r->segundo_apellido;
                $rol=$r->codigo_rol;
            }
            $resul=true;
        }else{
            $resul=false;
        }
            return response()->json([
                "RES"=>$resul,
                "id"=>$id,
                "usuario"=>$usuario,
                "nombres" =>$nombres,
                "p_apellido" =>$p_apellido,
                "s_apellido" => $s_apellido,
                "rol"=>$rol]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $date = Carbon::now();
        $usuario = DB::select("select proc_actualizar_usuario(?,?,?,?,?,?,?,?)",[$request->id,$request->usuario,$date,$request->nombres,$request->p_apellido,$request->s_apellido,$request->codigo_rol,Session::get('usuario')]);
        $usuario_="";
        foreach ($usuario as $r) {
            $usuario_=$r->proc_actualizar_usuario;
        }
        if($usuario_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    public function delete(Request $r){
         $usuario = DB::select("select proc_eliminar_usuario(?)",[$r->id]);
         $usuario_="";
        foreach ($usuario as $r) {
            $usuario_=$r->proc_eliminar_usuario;
        }
        if($usuario_=="OK"){
            return response()->json(["RES"=>true]);
        }else{
            return response()->json(["RES"=>false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

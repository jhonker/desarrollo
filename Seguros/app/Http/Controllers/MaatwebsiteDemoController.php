<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use Carbon\Carbon;

use DB;

use Excel;

use Session;

use Redirect;

class MaatwebsiteDemoController extends Controller

{

	public function importExport()

	{

		return view('importExport');

	}

	public function downloadExcelClient($type)

	{
		//$data = Item::get()->toArray();
		$data =DB::table("view_lb_clientes")->get();

		return Excel::create('clientes', function($excel) use ($data) {

			$excel->sheet('mySheet', function($sheet) use ($data)

	        {	
				$data= json_decode( json_encode($data), true);
				$sheet->fromArray($data);
	        });

		})->download($type);

	}

	public function downloadExcelVenta($type)

	{
		$titular =DB::select("select '1' TIPO_REGISTRO, b.genero GENERO,
		a.numero_identificacion NUMERO_DOCUMENTO, a.nombres NOMBRES,
		b.fecha_nacimiento FECHA_NACIMIENTO, a.fecha_adicion::date FECHA_INCLUSION,
		'TITULAR' PARENTESCO, b.numero_identificacion CEDULA_CLIENTE,
		c.codigo_cuenta CUENTA, 'LABPALC' USUARIO 
		from lb_clientes_x_seguro a, 
		lb_clientes b, lb_cuentas_x_cliente c, lb_seguros d
		where a.numero_identificacion= b.numero_identificacion
			and a.codigo_cuenta = c.codigo_cuenta 
			and b.codigo_cliente = c.codigo_cliente 
			and a.situacion = 'A'
			and a.mes_anio_inicio=  d.mes_anio_siguiente");
		foreach ($titular as $t) {
				$ff = date("d/m/Y", strtotime($t->fecha_nacimiento));
				$ff2 = date("d/m/Y", strtotime($t->fecha_inclusion));
				$dependientes =DB::select("SELECT '2' TIPO_REGISTRO, r.genero GENERO,
		 		r.numero_identificacion NUMERO_DOCUMENTO, r.nombres NOMBRES, r.fecha_nacimiento::date FECHA_NACIMIENTO,
		 		r.fecha_ingreso::date FECHA_INCLUSION, r.parentezco PARENTESCO, r.numero_identificacion_titular CEDULA_CLIENTE,
		 		'' CUENTA , 'LABPALC'  USUARIO
   				from lb_clientes_dependientes r
   				where r.numero_identificacion_titular =?",[$t->numero_documento]);
				$resources[] =[
								"TIPO_REGISTRO" => $t->tipo_registro,
								"GENERO" => trim($t->genero),
								"NUMERO_DOCUMENTO" => $t->numero_documento,
								"NOMBRES" => $t->nombres,
								"FECHA_NACIMIENTO" => $ff,
								"FECHA_INCLUSION" => $ff2,
								"PARENTESCO" => trim($t->parentesco),
								"CEDULA_CLIENTE_BCM" => $t->cedula_cliente,
								"CUENTA" => $t->cuenta,
								"USUARIO" => $t->usuario,
							];

					foreach ($dependientes as $dep) {
						$ff1 = date("d/m/Y", strtotime($dep->fecha_nacimiento));
						$ff21 = date("d/m/Y", strtotime($dep->fecha_inclusion));
						if($t->numero_documento == $dep->cedula_cliente){
							$resources1= [
											"TIPO_REGISTRO" => $dep->tipo_registro,
											"GENERO" => trim($dep->genero),
											"NUMERO_DOCUMENTO" => $dep->numero_documento,
											"NOMBRES" => $dep->nombres,
											"FECHA_NACIMIENTO" => $ff1,
											"FECHA_INCLUSION" => $ff21,
											"PARENTESCO" => trim($dep->parentesco),
											"CEDULA_CLIENTE_BCM" => $dep->cedula_cliente,
											"CUENTA" => $dep->cuenta,
											"USUARIO" => $dep->usuario,
								];

								array_push($resources,$resources1);

						}
				}
		}
			return Excel::create('ventas', function($excel) use ($resources) {
				$excel->sheet('mySheet', function($sheet) use ($resources)
						{
							$resources= json_decode( json_encode($resources), true);
							$sheet->fromArray($resources);
						});
			})->download($type);
	}
	public function importExcelClient()

	{

		if(Input::hasFile('import_file')){

			$path = Input::file('import_file')->getRealPath();

			$data = Excel::load($path, function($reader) {

			})->get();

			if(!empty($data) && $data->count()){

				foreach ($data as $key => $value) {
					$ff = date("Y-m-d", strtotime($value->fecha_de_nacimiento));
					$insert[] = [
									'numero_identificacion' => $value->numero_identificacion, 
									'nombres' => $value->nombres,
									'fecha_nacimiento' => $ff,
									'telefono' => $value->telefonos,
									'celular' => $value->celular_principal,
									'correo_electronico' => $value->correo_electronico,
									'direccion' => $value->direccion,
									'ciudad' => $value->ciudad,
									'provincia' => $value->provincia,
									'genero' => $value->genero,
									'codigo_cuenta' => $value->codigo_cuenta,
									'tipo_cuenta' => $value->tipo_producto 
								];
					//echo $value;
				}

				if(!empty($insert)){

					$sqlInsert = json_encode($insert);

					$roles =DB::select('select proc_registrar_clientes(?,?,?)',[$sqlInsert,date("Y-m-d"),Session::get('usuario')]);


				}

			}

		}

		return back();
        //return response()->json(["RES"=>true]);


	}

	public function importarCaptaciones()

	{

		if(Input::hasFile('import_captaciones')){

			$path = Input::file('import_captaciones')->getRealPath();

			$data = Excel::load($path, function($reader) {

			})->get();

			if(!empty($data) && $data->count()){

				foreach ($data as $key => $value) {
					$ff = date("Y-m-d", strtotime($value->fecha_inclusion));
					$ff2 = date("Y-m-d", strtotime($value->fecha_nacimiento));
					$tipo = $value->tipo_registro;
					$nn ='';
					if($tipo==1){
						$nn = trim($value->primer_apellido).' '.trim($value->segundo_apellido).' '.trim($value->nombres);
					}else{
						$nn = $value->nombres;
					}
					$insert[] = [
									'tipo_registro' => $value->tipo_registro, 
									'genero' => $value->genero,
									'tipo_documento' => $value->tipo_documento,
									'numero_documento' => $value->numero_documento,

									'nombres' => $nn,
									'fecha_nacimiento' => $ff2,
									'fecha_inclusion' => $ff,
									'caregoria_del_asegurado' => $value->caregoria_del_asegurado,
									'primera_cobrada' => $value->primera_cobrada,
									'cuenta' => $value->cuenta,
									'parentesco' => $value->parentesco
								];
					//echo $value;
				}

				if(!empty($insert)){

					$sqlInsert = json_encode($insert);
					//echo $sqlInsert;
					$roles =DB::select('select proc_captaciones(?,?,?)',[$sqlInsert,date("Y-m-d"),Session::get('usuario')]);
					//echo json_encode($roles);
					//dd('Insert Record successfully.');

				}

			}

		}

		return back();
        //return response()->json(["RES"=>true]);


	}

	public function importarCarteraCaptaciones()

	{

		if(Input::hasFile('import_cartera_captaciones')){

			$path = Input::file('import_cartera_captaciones')->getRealPath();

			$data = Excel::load($path, function($reader) {

			})->get();

			if(!empty($data) && $data->count()){

				foreach ($data as $key => $value) {

					$ff = date("Y-m-d", strtotime($value->fecha_inclusion));
					$ff2 = date("Y-m-d", strtotime($value->fecha_nacimiento));
					$tipo = $value->tipo_registro;
					$nn ='';
					if($tipo==1){
						$nn = trim($value->primer_apellido).' '.trim($value->segundo_apellido).' '.trim($value->nombres);
					}else{
						$nn = $value->nombres;
					}
					$insert[] = [
									'tipo_registro' => $value->tipo_registro, 
									'genero' => $value->genero,
									'tipo_documento' => $value->tipo_documento,
									'numero_documento' => $value->numero_documento,

									'nombres' => $nn,
									'fecha_nacimiento' => $ff2,
									'fecha_inclusion' => $ff,
									'caregoria_del_asegurado' => $value->caregoria_del_asegurado,
									'primera_cobrada' => $value->primera_cobrada,
									'cuenta' => $value->cuenta ,
									'parentesco' => $value->parentesco
								];
					//echo $value;
				}

				if(!empty($insert)){

					$sqlInsert = json_encode($insert);
					//echo $sqlInsert;
					$roles =DB::select('select proc_cartera(?,?,?)',[$sqlInsert,date("Y-m-d"),Session::get('usuario')]);
					//echo json_encode($roles);
					//dd('Insert Record successfully.');

				}

			}

		}

		return back();
        //return response()->json(["RES"=>true]);


	}


	public function importarCancelaciones()

	{

		if(Input::hasFile('import_cancelaciones')){

			$path = Input::file('import_cancelaciones')->getRealPath();

			$data = Excel::load($path, function($reader) {

			})->get();

			if(!empty($data) && $data->count()){

				foreach ($data as $key => $value) {

					$ff = date("Y-m-d", strtotime($value->fecha_inclusion));
					$ff2 = date("Y-m-d", strtotime($value->fecha_nacimiento));
					$ff3 = date("Y-m-d", strtotime($value->fecha_cancelacion));
					$tipo = $value->tipo_registro;
					$nn ='';
					if($tipo==1){
						$nn = trim($value->primer_apellido).' '.trim($value->segundo_apellido).' '.trim($value->nombres);
					}else{
						$nn = $value->nombres;
					}
					$insert[] = [
									'tipo_registro' => $value->tipo_registro, 
									'genero' => $value->genero,
									'tipo_documento' => $value->tipo_documento,
									'numero_documento' => $value->numero_documento,

									'nombres' => $nn,
									'fecha_nacimiento' => $ff2,
									'fecha_inclusion' => $ff,
									'fecha_cancelacion' => $ff3,
									'caregoria_del_asegurado' => $value->caregoria_del_asegurado,
									'primera_cobrada' => $value->primera_cobrada,
									'cuenta' => $value->cuenta ,
									'parentesco' => $value->parentesco,
									'observacion' => $value->observacion
								];
					//echo $value;
				}

				if(!empty($insert)){

					$sqlInsert = json_encode($insert);
					//echo $sqlInsert;
					$roles =DB::select('select proc_cancelacion(?,?,?)',[$sqlInsert,date("Y-m-d"),Session::get('usuario')]);
					//echo json_encode($roles);
					//dd('Insert Record successfully.');

				}

			}

		}

		return back();
        //return response()->json(["RES"=>true]);


	}
}
?>
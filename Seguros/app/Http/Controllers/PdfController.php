<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

use Carbon\Carbon;

use DB;

use PDF;

use Session;

use Redirect;

class PdfController extends Controller

{
    public function generatePdfCliente() {
        $data =DB::table("view_lb_clientes")->get();
        $pdf = PDF::loadView('pdf.document',compact('data'), [],[
            'format' => 'A4-L'
        ]);
        return $pdf->download('clientes.pdf');
    }

    public function generatePdfVenta() {
        $data =DB::table("view_lb_clientes")->get();
        $pdf = PDF::loadView('pdf.document',compact('data'), [],[
            'format' => 'A4-L'
        ]);
        return $pdf->download('ventas.pdf');
    }
}
?>
$(document).ready(function(){
	var cedula= $("#identificacion").html();
	var cuenta = $("#c_cuenta").val();
	get_cuentas(cedula,cuenta);
	$("#cbm_seguros").val(1);
	get_resul_seguro(1);
    //fn_dar_eliminar();
})

$("#edit_datos").click(function(){
var telefono = $("#telefono").html();
var celular = $("#celular").html();
var correo = $("#correo").html();

$("#sp_telefono").html('<input type="text" id="telefono_e" class="form-control" value="'+telefono+'">');
$("#sp_celular").html('<input type="text" id="celular_e" class="form-control" value="'+celular+'">');
$("#sp_correo").html('<input type="text" id="correo_e" class="form-control" value="'+correo+'">');
$("#edit_datos").hide();
$("#save_datos").show();

});

$("#save_datos").click(function(){
	var telefono = $("#telefono_e").val();
	var celular = $("#celular_e").val();
	var correo = $("#correo_e").val();
	var token   = $("#token").val();
	var codigo = $("#codigo").html();
	$.ajax({
	url:'/update_datos_cliente',
	type:'POST',
	dataType:'json',
	headers :{'X-CSRF-TOKEN': token},
	data:{
		codigo:codigo,
		telefono:telefono,
		celular:celular,
		correo:correo
	},
	success:function(res){
		if(res.RES){
			alert("Datos Actualizados");
			$("#sp_telefono").html('<span id="telefono">'+telefono+'</span>');
			$("#sp_celular").html('<span id="celular">'+celular+'</span>');
			$("#sp_correo").html('<span id="correo">'+correo+'</span>');
			$("#edit_datos").show();
			$("#save_datos").hide();
		}
	}
	})
});
/*$("#b_añadir").click(function(){
	var identificacion = $("#identificacion_dep").val();
	var nombres = $("#nombres_dep").val();
	var f_nacimiento = $("#txt-date").val();
	var genero = $("#genero_dep").val();
	var parentesco =$("#parentesco").val();
	if(identificacion=="" && nombres=="" && f_nacimiento=="" &&genero==0 &&parentesco==0){
		alert("No se pudo añadir depenencia campos vacios");
		$("#identificacion_dep").focus();
	}else if(identificacion==""){
		$("#identificacion_dep").focus();
	}else if(nombres==""){
		$("#nombres_dep").focus();
	}else if(f_nacimiento==""){
 		$("#txt-date").focus();
	}else if(genero==0){

	}else if(parentesco==0){

	}else{
	 nfila = $("#tabla_dependencia  tbody").find("tr").length; // contamos los numero de filas (tr)
     nfila = nfila + 1;
     debugger
    var y = 0;
    for (var x = 1; x <= nfila; x++) {
        if ($("#t_identificacion" + x + "").val() == identificacion) {
                cant_ = parseFloat($('#Tcantidad' + x + '').val()) + 1;
                y = y + 1;
            }
        } //FIN DEL FOR

   		if (y == 0) {
            cadena = "<tr>";
            cadena = cadena + "<td>" + nfila + "</td>";
            cadena = cadena + "<td> <span id='t_identificacion"+nfila+"'>"+identificacion+"</span></td>";
            cadena = cadena + "<td> <span id='nombres"+nfila+"'>" + nombres + "</span></td>";
            cadena = cadena + "<td> <span id='f_naci"+nfila+"'>" + f_nacimiento + "</span></td>";
            cadena = cadena + "<td> <span id='genero"+nfila+"'>" + genero+ " </span> </td>";
            cadena = cadena + "<td> <span id='parentesco"+nfila+"'>" + parentesco+ " </span> </td>";
            cadena = cadena + "<td><a class='elimina'><img class='delete' src='/dist/remove.png' /></a></td>";
            $("#tabla_dependencia tbody").append(cadena);

            $("#identificacion_dep").val("");
	 		$("#nombres_dep").val("");
	 		$("#txt-date").val("");
	 		$("#genero_dep").val(0);
			$("#parentesco").val(0);
        }else{
            alert("El pariente: "+nombres +" ya esta agregado");
        }

        fn_dar_eliminar();
	}

})*/


function get_resul_seguro(id){
	$.ajax({
		url:'/get_resul_seguro/'+id,
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				$("#precio_seguro").val(v.valor);
			});
		}
	})
}

function get_cuentas(identificacion,cuenta){

	var combo="<option value='0'>Selecciones la cuenta</option>";
	$.ajax({
		url:'/get_cuentas/'+identificacion,
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				combo+="<option value='"+v.codigo_cuenta+"'>"+v.nombre_cuenta+"</option>";
			});
				$("#cbm_cuentas").html(combo);
				$("#cbm_cuentas").val(cuenta);
				get_cuenta(cuenta);
		}

	})
}

$("#cbm_cuentas").change(function(){
	var codigo_cuenta=$(this).val();
	$.ajax({
		url:'/get_tipo_cuenta/'+codigo_cuenta,
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				$("#tipo_cuenta").html(v.tipo_cuenta+" - "+codigo_cuenta);
				$("#codigo_cuenta").val(codigo_cuenta);
			});
		}
	})
});

function get_cuenta(codigo_cuenta){
	$.ajax({
		url:'/get_tipo_cuenta/'+codigo_cuenta,
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				$("#tipo_cuenta").html(v.tipo_cuenta+" - "+codigo_cuenta);
				$("#codigo_cuenta").val(codigo_cuenta);
			});
		}
	})
}


function fn_dar_eliminar() {
    $("a.elimina").click(function() {
        id = $(this).parents("tr").find("td").eq(0).html();
        	$(this).parents("tr").fadeOut("normal", function() {
             	$(this).remove();
            });
    });
};




/*FUNCIONES PARA DEPENDIENTES*/

$("#b_añadir").click(function(){
	var identificacion = $("#identificacion_dep").val();
	var nombres = $("#nombres_dep").val();
	var f_nacimiento = $("#txt-date").val();
	var genero = $("#genero_dep").val();
	var parentesco =$("#parentesco").val();
	var titular = $("#identificacion").html();
	if(identificacion=="" && nombres=="" && f_nacimiento=="" &&genero==0 &&parentesco==0){
		alert("No se pudo añadir depenencia campos vacios");
		$("#identificacion_dep").focus();
	}else if(identificacion==""){
		$("#identificacion_dep").focus();
	}else if(nombres==""){
		$("#nombres_dep").focus();
	}else if(f_nacimiento==""){
 		$("#txt-date").focus();
	}else if(genero==0){

	}else if(parentesco==0){

	}else{
	AddDependientes(titular,identificacion,nombres,f_nacimiento,genero,parentesco);
	/* nfila = $("#tabla_dependencia  tbody").find("tr").length; // contamos los numero de filas (tr)
     nfila = nfila + 1;
    var y = 0;
    for (var x = 1; x <= nfila; x++) {
        if ($("#t_identificacion" + x + "").val() == identificacion) {
                cant_ = parseFloat($('#Tcantidad' + x + '').val()) + 1;
                y = y + 1;
            }
        } //FIN DEL FOR

   		if (y == 0) {
            cadena = "<tr>";
            cadena = cadena + "<td>" + nfila + "</td>";
            cadena = cadena + "<td> <span id='t_identificacion"+nfila+"'>"+identificacion+"</span></td>";
            cadena = cadena + "<td> <span id='nombres"+nfila+"'>" + nombres + "</span></td>";
            cadena = cadena + "<td> <span id='f_naci"+nfila+"'>" + f_nacimiento + "</span></td>";
            cadena = cadena + "<td> <span id='genero"+nfila+"'>" + genero+ " </span> </td>";
            cadena = cadena + "<td> <span id='parentesco"+nfila+"'>" + parentesco+ " </span> </td>";
            cadena = cadena + "<td><a class='elimina'><img class='delete' src='/dist/remove.png' /></a></td>";
            $("#tabla_dependencia tbody").append(cadena);

            $("#identificacion_dep").val("");
	 		$("#nombres_dep").val("");
	 		$("#txt-date").val("");
	 		$("#genero_dep").val(0);
			$("#parentesco").val(0);
        }else{
            alert("El pariente: "+nombres +" ya esta agregado");
        }

        fn_dar_eliminar();*/
	}
});

function AddDependientes(titular,identificacion,nombres,f_nacimiento,genero,parentesco){
	var token   = $("#token").val();
	$.ajax({
		url:'/AddDependientes',
		type:'POST',
		headers :{'X-CSRF-TOKEN': token},
		dataType:'json',
		data:{
			titular:titular,
			identificacion:identificacion,
			nombres:nombres,
			f_nacimiento:f_nacimiento,
			genero:genero,
			parentesco:parentesco,
		},
		success:function(res){
			if(res.RES){
					get_tabla_dependientes(titular);
					limpiar_dependientes();
					}
		}
	})
}

function get_tabla_dependientes(titular){
	$.ajax({
		url:'/get_dependientes/'+titular,
					type:'GET',
					dataType:'json',
					success:function(res){
					var tbody1 = "";
					$(res).each(function(i, v){
						tbody1 +="<tr><td>"+i+"</td><td>"+v.numero_identificacion+"</td>";
						tbody1 +="<td>"+v.nombres+"</td>";
						tbody1 +="<td>"+v.fecha_nacimiento+"</td>";
						tbody1 +="<td>"+v.genero+"</td>";
						tbody1 +="<td>"+v.parentezco+"</td><td><a class='elimina' onclick='deleteDependiente("+v.codigo_cliente_dependiente+")'><img class='delete' src='/dist/remove.png' /></a><td></tr>";
					});
					$("#tbody_dependientes").html(tbody1);
				}
			})
}

function limpiar_dependientes(){
	 $("#identificacion_dep").val(""); $("#nombres_dep").val(""); $("#txt-date").val(""); $("#genero_dep").val(0); $("#parentesco").val(0);
}
/*function add_dependientes(cedula){
	$("#cedula_titular").val(cedula);
	$.ajax({
		url:'/get_dependientes/'+cedula,
		type:'GET',
		dataType:'json',
		success:function(res){

			var tbody1 = "";
			console.log(res.length);

			$(res).each(function(i, v){
				tbody1 +="<tr><td>"+i+"</td><td>"+v.numero_identificacion+"</td>";
				tbody1 +="<td>"+v.nombres+"</td>";
				tbody1 +="<td>"+v.fecha_nacimiento+"</td>";
				tbody1 +="<td>"+v.genero+"</td>";
				tbody1 +="<td>"+v.parentezco+"</td><td><a class='elimina' onclick='deleteDependiente("+v.codigo_cliente_dependiente+")'><img class='delete' src='/dist/remove.png'/></a><td></tr>";
			});
			console.log(tbody1);
			$("#tbody_dependientes").html(tbody1);
			$("#modal_agregar_dependientes").modal("show");
		}
	})
}*/

$("#btn_update_venta").click(function(){
	var token   = $("#token").val();
	var in_numero_identificacion = $("#identificacion").html();
	var in_codigo_cuenta = $("#codigo_cuenta").val();
	debugger
	$.ajax({
		url:'/update_venta',
		type:'POST',
		dataType:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			in_numero_identificacion:in_numero_identificacion,
			in_codigo_cuenta:in_codigo_cuenta
		},
		success:function(res){
			if(res.RES){
				alert("Venta se modifico correctamente");
			}else{
				alert(res.RESP);
			}
		}

	})
	
});


function deleteDependiente(id){
	var titular = $("#identificacion").html();
	var token   = $("#token").val();
	swal({ title: "LAB-Palacios",
      text: "Esta Seguro de Eliminar este dependiente!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#4CAF50",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: true,
      closeOnCancel: true},
      function(isConfirm){
        if (isConfirm) {
        	$.ajax({
        		url:'/delete_dependiente',
        		type:'POST',
        		dataType:'json',
				headers :{'X-CSRF-TOKEN': token},
        		data:{
        			id:id
        		},
        		success:function(res){
        			if(res.RES){
						get_tabla_dependientes(titular);
        				swal("El dependiente a sido eliminado", "success");
        			}
        		}
        	})
        }
    });
}


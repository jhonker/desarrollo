var AJ_AX='';
var arraayDependiente=[]
$("#nombres").keyup(function(){
	//$("#modal_cliente_search").modal("show");
	if(this.value.length > 5){
		searchClient()
	}else{
		$("#div-search-hd").hide()
	}
	
});
var data_js ='';
var id_search =''
function searchClient(){
    $("#div-result").html('')
    $("#div-search-hd").html('buscando..')
    $("#div-search-hd").show()
	var token   = $("#token").val();
	if(AJ_AX && AJ_AX.readyState != 4){
		AJ_AX.abort();
		AJ_AX = null;
	  }
	  AJ_AX = $.ajax({
		url:'clientes/seguros',
		type:'POST',
		dataType:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			id:$("#nombres").val()

		},
		success:function(res){
			var ht ='';
			if(res.RES){
                ht ='<table class="table">'
                data_js = res.cliente
				$(res.cliente).each(function(i, v){
					ht +="<tr>";
					ht +="<td>"+v.codigo_cliente+"</td>";
					ht +="<td>"+v.nombres+"</td>";
					ht +="<td><button type='button' class='btn btn-info' onclick='seleccionar("+i+")'>Seleccionar</button> </td> </tr>";
				});
				ht +="</tbody> </table>";
				$("#div-search-hd").html(ht);
				
				
			}else{
				swal("El Cliente no se encontro", "error","danger");
				$("#div-search-hd").hide()
			}
			
		}
	})
}

function seleccionar(id){
  
    $("#div-search-hd").hide()
    console.log(id)
    $(data_js).each(function(i, v){
        if(i == id){
            var ht ='<div class="col-12 col-md-6 offset-md-3"><input type="hidden" id="codigoCliente" value="'+v.codigo_cliente+'" /><input type="hidden" id="nombreCliente" value="'+v.nombres+'" /><table class="table">'
                ht +='  <tr>'
                ht +='      <td><strong>Cliente:</strong></td>'
                ht +='      <td>'+v.codigo_cliente+'</td>'
                ht +='  </tr>'
                ht +='  <tr>'
                ht +='      <td><strong>Nombres y Apellidos:</strong></td>'
                ht +='      <td>'+v.nombres+'</td>'
                ht +='  </tr>'
                ht +='  <tr>'
                ht +='      <td><strong>Estado del Seguro:</strong></td>'
                ht +='      <td>'+v.situacion+'</td>'
                ht +='  </tr>'
                ht +='  <tr>'
                ht +='      <td><strong>Fecha Activación del Seguro:</strong></td>'
                ht +='      <td>'+v.fecha_adicion+'</td>'
                ht +='  </tr>'
                ht +='  <tr>'
                ht +='      <td><strong>Ultimo mes pagado:</strong></td>'
                ht +='      <td>'+v.ultimo_mes_cobrado+'</td>'
                ht +='  </tr>'
                ht +='  <tr>'
                ht +='      <td><strong>Total Acumulado:</strong></td>'
                ht +='      <td>'+v.total_acumulado+'</td>'
                ht +='  </tr>'
                ht +='  <tr>'
                ht +='      <td><strong>Fecha Cancelacion:</strong></td>'
                ht +='      <td>'+v.fecha_cancelacion  +' | Observacion: '+v.observacion +'</td>'
                ht +='  </tr>'
                ht +='  <tr>'
                ht +='      <td><strong>Fecha Reapertura:</strong></td>'
                ht +='      <td>'+ v.fecha_reapertura +'</td>'
                ht +='  </tr>'
                ht +='  <tr>'
                ht +='      <td><strong>Total meses Cobrados:</strong></td>'
                ht +='      <td>'+v.meses_cobrados+'</td>'
                ht +='  </tr>'
                ht +='  <tr>'
                ht +='      <td><strong>Producto:</strong></td>'
                ht +='      <td>'+v.tipo_producto+'</td>'
                ht +='  </tr>'
                if(v.servicio_utilizado > 0){
                    ht +='  <tr>'
                    ht +='      <td><strong>Servicios Utilizados:</strong></td>'
                    ht +='      <td>'+v.servicio_utilizado+'  <a href="#" id="btn-ver" >ver visitas</a></td>'
                    ht +='  </tr>'
                }else{
                    ht +='  <tr>'
                    ht +='      <td><strong>Servicios Utilizados:</strong></td>'
                    ht +='      <td>'+v.servicio_utilizado+'</td>'
                    ht +='  </tr>'
                }
                
                ht +='  <tr>'
                ht +='      <td colspan="2"><button type="button" class="btn btn-success pull-right" id="btn-visita">Agregar Visita <i class="fa fa-plus"></i></button></td>'
                ht +='  </tr>'
                
                ht +='</table></div>'
                $("#div-result").html(ht)

                $("#btn-visita").click(function(){
                    agregarVisita()
                })

                $("#btn-ver").click(function(){
                    cargarVisitas()
                })
            }
        });
}
function cargarVisitas(){
    var token   = $("#token").val();
    $("#modal_visitas_ver").modal("show")
    
    $("#tbl-visitas").html('<i class="fa fa-spinner fa-spin fa-fw"></i>')
    $.ajax({
        url:'/administrar-visitas/toma',
        type:'POST',
        dataType:'json',
        headers :{'X-CSRF-TOKEN': token},
        data:{
            id:$("#codigoCliente").val()
        },

        success:function(res){
            var ht ='<table class="table">';
                ht += '<tr>'
                ht += ' <td>PERSONA QUE SE HIZO ATENDER</td>'
                ht += ' <td>CEDULA</td>'
                ht += ' <td>TIPO SERVICIO</td>'
                ht += ' <td>FECHA DE ATENCIÓN</td>'
                ht += '</tr>'
            if(res.RES){
               
                $(res.dependientes).each(function(i, v){
                    ht += '<tr>'
                    ht += '   <td>'+v.nombre_dependiente+'</td>'
                    ht += '   <td>'+v.numero_identificacion+'</td>'
                    if(v.tipo_servicio == 1){
                        ht += '   <td>EXAMENES MEDICOS</td>'
                    }else{
                        ht += '   <td>SERVICIOS ODONTOLOGICOS</td>'
                    }
                    ht += '   <td>'+v.fecha_ingreso+'</td>'

                    ht += '</tr>'
                })
                ht +='</table>';
                $("#tbl-visitas").html(ht)

            }else{
                $("#modal_visitas_ver").modal("hide")
                swal("Alerta","No hay datos", "danger");
            }


        },fail:function(res){
            $("#modal_visitas_ver").modal("hide")
            swal("Alerta","No hay datos", "danger");
        }
    })
}

function agregarVisita(){
    arraayDependiente=[]
    var token   = $("#token").val();
    var id = $("#codigoCliente").val()
    $("#modal_visitas").modal("show")
    
    $("#div-seguro").html('<i class="fa fa-spinner fa-spin fa-fw"></i>')
    $.ajax({
        url:'clientes/dependientes',
        type:'POST',
        dataType:'json',
        headers :{'X-CSRF-TOKEN': token},
        data:{
            id:id
        },
        success:function(res){
            var ht ='<table class="table">';
            ht += '<tr>'
                ht += '   <td>'
                ht += '      <div class="checkbox"><label>  <input class="check-dependiente" type="checkbox" id="'+$("#codigoCliente").val()+'">';
                ht += $("#nombreCliente").val()
                ht += '   </label></div> </td>'
                ht += '</tr>'
            if(res.RES){
               
                $(res.dependientes).each(function(i, v){
                    ht += '<tr>'
                    ht += '   <td>'
                    ht += '      <div class="checkbox"><label>  <input class="check-dependiente" type="checkbox" id="'+v.numero_identificacion+'">';
                    if(v.primer_apellido=!null && v.segundo_apellido!=null){
                    ht +=v.primer_apellido+' '+v.segundo_apellido+' '+v.nombres
                    }else{
                        ht += v.nombres+' '
                    }
                    ht += '   </label></div> </td>'
                    ht += '</tr>'
                })
                ht +='</table>';
                
                $("#div-seguro").html(ht)
                $(".check-dependiente").change(function(){
                    cargarArrayDependiente(this.id,this)
                })
            }else{
                ht +='</table>';
                
                $("#div-seguro").html(ht)
                $(".check-dependiente").change(function(){
                    cargarArrayDependiente(this.id,this)
                })
            }


        },fail:function(res){

            swal("Error, recargue la página!", "danger");
        }
    })

}

function cargarArrayDependiente(id,th){


            if( $(th).prop('checked') ) {

                    if ((jQuery.inArray( id, arraayDependiente ) == -1)) {

                        arraayDependiente.push(id)
                        console.log(arraayDependiente)

                    }
            }else{
 
                var idx =returnIndexArr(arraayDependiente,id)
                removeItemFromINDEX(arraayDependiente,idx)
                console.log(arraayDependiente)
            }
}

$("#btn-save-visita").click(function(){
    if(arraayDependiente.length>0){
        console.log("entro")
        var token   = $("#token").val();
        var id = $("#codigoCliente").val()
        $("#btn-save-visita").html('<i class="fa fa-spinner fa-spin fa-fw"></i>')
        $("#btn-save-visita").attr('disabled',true);
        
        $.ajax({
            url:'/administrar-visitas/agregar',
            type:'POST',
            dataType:'json',
            headers :{'X-CSRF-TOKEN': token},
            data:{
                codigo_cliente:id,
                arraayDependiente:arraayDependiente,
                tipo : $("#select-seguro").val()
            },
            success:function(res){
                if(res.RES){
                    
                    var data = res.RES[0]
                    var datareturn = data["proc_registrar_visitas"]
                    if(datareturn=='1'){
                        $("#modal_visitas").modal("hide")
                        swal("Alerta","Visita agregada con exito!", "success");
                        
                        setTimeout(function(){ window.location.reload() }, 1000);
                    }else{
                        swal("Error, intentelo de nuevo!", "danger");
                    }
                    
                    
                    $("#btn-save-visita").html('Guardar Visita')
                    $("#btn-save-visita").attr('disabled',false);
                }else{
                    swal("Error, recargue la página!", "danger");
                    $("#btn-save-visita").html('Guardar Visita')
                    $("#btn-save-visita").attr('disabled',false);
                }
    
    
            },fail:function(res){
    
                swal("Error, recargue la página!", "danger");
                $("#btn-save-visita").html('Guardar Visita')
                $("#btn-save-visita").attr('disabled',false);
            }
        })
    }else{
        
        swal("selecione una persona!", "warning");
    }
})
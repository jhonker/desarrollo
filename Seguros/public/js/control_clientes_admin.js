$(document).ready(function(){
//	dataTable_clientes();
});
$("#btn_new_client").click(function(){
	$("#modal_client").modal("show");
});

$("#btn_client").click(function(){
	$("#modal_client_new").modal("show");
	
});
$("#btn-save-client").click(function(){
	//console.log("entro")
	addClientNew()
})

function delete_cliente(id){
	var token   = $("#token").val();
	swal({ title: "LAB-Palacios",
      text: "Esta Seguro de Eliminar este CLIENTE!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#4CAF50",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: true,
      closeOnCancel: true},
      function(isConfirm){
        if (isConfirm) {
        	$.ajax({
        		url:'clientes/eliminar-clientes',
        		type:'POST',
        		dataType:'json',
				headers :{'X-CSRF-TOKEN': token},
        		data:{
        			id:id
        		},
        		success:function(res){
        			if(res.RES){
						swal("El Cliente a sido eliminado", "success");
        				get_tabla();
        				addEventLoadClient();
        			}
        		}
        	})
        }
    });
}

function update_cliente(id){
	$("#hidden1").hide();
	$("#hidden2").hide();
	$("#modal_client_update").modal('show');
	$("#div-hd").show();
    $.ajax({
        		url:'clientes/search/'+id,
        		type:'GET',
        		dataType:'json',
        		success:function(res){

					if(res.RES){
						$("#div-hd").hide();
						$("#hidden1").show();
						$("#hidden2").show();
						$(res.cliente).each(function(i, v){

							$("#txt-id").val(v.codigo_cliente)
							$("#txt-codigo2").val(v.numero_identificacion)
							$("#div-cl").html(v.nombres)
							$("#txt-names2").val(v.nombres)
							$("#txt-date2").val(v.fecha_nacimiento)
							$("#txt-telefono2").val(v.telefono)
							$("#txt-correo2").val(v.correo_electronico)
							$("#txt-celular2").val(v.celular)
							$("#txt-ciudad2").val(v.ciudad)
							$("#txt-provincia2").val(v.provincia)
							$("#txt-direccion2").val(v.direccion)
							$("#txt-genero2 option[value="+v.genero+"]").attr("selected",true);


						});
						$("#btn-update-client").click(function(){
							
							var token   = $("#token").val();
							var id = $("#txt-id").val();
							var codigo = $("#txt-codigo2").val();
							var names = $("#txt-names2").val();
							var date = $("#txt-date2").val();
							var telefono = $("#txt-telefono2").val();
							var celular = $("#txt-celular2").val();
							var provincia = $("#txt-provincia2").val();
							var ciudad = $("#txt-ciudad2").val();
							var direccion = $("#txt-direccion2").val();
							var genero = $("#txt-genero2").val();
							var correo = $("#txt-correo2").val();
							if(codigo!=""){
								if(names!=""){
									if(date!=""){
										if(telefono!=""){
											if(celular!=""){
												if(provincia!=""){
													if(ciudad!=""){
														if(direccion!=""){
															if(genero!=""){
	
																		if(correo!=""){
																			$("#btn-update-client").html('<i class="fa fa-spinner fa-spin fa-fw"></i>')
																			$("#btn-update-client").attr('disabled',true)
																			$.ajax({
																				url:'clientes/modificar-cliente',
																				type:'POST',
																				dataType:'json',
																				headers :{'X-CSRF-TOKEN': token},
																				data:{
																					codigo:codigo,
																					names:names,
																					date:date,
																					telefono:telefono,
																					celular:celular,
																					provincia:provincia,
																					ciudad:ciudad,
																					direccion:direccion,
																					genero:genero,
																					id:id,
																					correo:correo
						
																				},
																				success:function(res){
																					if(res.RES){
																						$("#btn-update-client").html('Guardar')
																						$("#btn-update-client").attr('disabled',false)
																						$("#modal_client_update").modal("hide");
																						get_tabla();
																						swal("El Cliente a sido modificado correctamente", "success");
																					}else{
																						swal("El Cliente no se ha modificado", "error");
																					}
																				}
																			})
																		}else{
																			$("#txt-correo2").focus()
																		}
						
															}else{
																$("#txt-genero2").focus()
															}
														}else{
															$("#txt-direccion2").focus()
														}
													}else{
														$("#txt-ciudad2").focus()
													}
												}else{
													$("#txt-provincia2").focus()
												}
											}else{
												$("#txt-celular2").focus()
											}
										}else{
											$("#txt-telefono2").focus()
										}
									}else{
										$("#txt-date2").focus()
									}
								}else{
									$("#txt-names2").focus()
								}
							}else{
								$("#txt-codigo2").focus()
							}
						})
					}else{
						$("#div-hd").hide();
						$("#modal_client_update").modal('hide');
						swal("Error al tomar datos del cliente.", "error");
					}
        		}
    })

}

function get_tabla(){
	var tabla="<table class='table datatable' ><thead><tr><th>CODIGO</th><th>IDENTIFICACIÓN</th><th>NOMBRES</th><th>FECHA NACIMIENTO</th><th>TELEFONO</th><th>CELULAR</th><th>CIUDAD</th><th>DIRECCION</th><th></th></tr></thead> <tbody>"
	$.ajax({
		url:'clientes/listado-cliente',
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				tabla +="<tr>";
				tabla +="<td>"+v.codigo_cliente+"</td>";
				tabla +="<td>"+v.numero_identificacion+"</td>";
				tabla +="<td>"+v.nombres+"</td>";
				tabla +="<td>"+v.fecha_nacimiento+"</td>";
				tabla +="<td>"+v.telefono+"</td>";
				tabla +="<td>"+v.celular+"</td>";
				tabla +="<td>"+v.ciudad+"</td>";
				tabla +="<td>"+v.direccion+"</td>";
				tabla +="<td><div class='btn-group'><a href='#'' data-toggle='dropdown' class='btn btn-primary dropdown-toggle'>Opciones <span class='caret'></span></a>";
				tabla +="<ul class='dropdown-menu' role='menu'>";
				tabla +="<li><a href='#' onclick='update_cliente("+v.codigo_cliente+"')>Modificar</a></li> <li><a href='#' onclick='delete_cliente("+v.codigo_cliente+")'>Eliminar</a></li>";
				tabla +="</ul> </div> </td> </tr>";
			});
			tabla +="</tbody> </table>";

			$("#tabla").html(tabla);
			dataTable_clientes();
		}
	})
}



function addClientNew(){
	
	var token   = $("#token").val();
	var codigo = $("#txt-codigo").val();
	var names = $("#txt-names").val();
	var date = $("#txt-date").val();
	var telefono = $("#txt-telefono").val();
	var celular = $("#txt-celular").val();
	var provincia = $("#txt-provincia").val();
	var ciudad = $("#txt-ciudad").val();
	var direccion = $("#txt-direccion").val();
	var genero = $("#txt-genero").val();
	var tipo = $("#txt-tipo-cuenta").val();
	var cuenta = $("#txt-cuenta").val();
	var correo = $("#txt-correo").val();
	if(codigo!=""){
		if(names!=""){
			if(date!=""){
				if(telefono!=""){
					if(celular!=""){
						if(provincia!=""){
							if(ciudad!=""){
								if(direccion!=""){
									if(genero!=""){
										if(tipo!=""){
											if(cuenta!=""){
												if(correo!=""){
													$("#btn-save-client").html('<i class="fa fa-spinner fa-spin fa-fw"></i>')
													$("#btn-save-client").attr('disabled',true)
													$.ajax({
														url:'clientes/agregar-cliente',
														type:'POST',
														dataType:'json',
														headers :{'X-CSRF-TOKEN': token},
														data:{
															codigo:codigo,
															names:names,
															date:date,
															telefono:telefono,
															celular:celular,
															provincia:provincia,
															ciudad:ciudad,
															direccion:direccion,
															genero:genero,
															tipo:tipo,
															cuenta:cuenta,
															correo:correo

														},
														success:function(res){
															if(res.RES){
																$("#btn-save-client").html('Guardar')
																$("#btn-save-client").attr('disabled',false)
																$("#modal_client_new").modal("hide");
																/*get_tabla();
																addEventLoadClient();*/
																swal("El Cliente a sido agregado correctamente", "success");
															}else{
																swal("El Cliente no se ha agregado correctamente", "error");
															}
														}
													})
												}else{
													$("#txt-correo").focus()
												}
											}else{
												$("#txt-cuenta").focus()
											}
										}else{
											$("#txt-tipo-cuenta").focus()
										}
									}else{
										$("#txt-genero").focus()
									}
								}else{
									$("#txt-direccion").focus()
								}
							}else{
								$("#txt-ciudad").focus()
							}
						}else{
							$("#txt-provincia").focus()
						}
					}else{
						$("#txt-celular").focus()
					}
				}else{
					$("#txt-telefono").focus()
				}
			}else{
				$("#txt-date").focus()
			}
		}else{
			$("#txt-names").focus()
		}
	}else{
		$("#txt-codigo").focus()
	}
};


/*$(document).ready(function() {
	addEventLoadClient()
});
function addEventLoadClient(){
	console.log($("#last-id").val())
	var token   = $("#token").val();
	//$( ".next").unbind( "click" );
	$(".next").click(function(){
		$.ajax({
			url:'/clientes/listado-cliente-load',
			type:'POST',
			dataType:'json',
			headers :{'X-CSRF-TOKEN': token},
			data:{
				limits:$("#last-id").val(),
			},
			success:function(res){
				var tabla ='';
				$(res).each(function(i, v){
					tabla +="<tr>";
					tabla +="<td>"+v.codigo_cliente+"</td>";
					tabla +="<td>"+v.numero_identificacion+"</td>";
					tabla +="<td>"+v.nombres+"</td>";
					tabla +="<td>"+v.fecha_nacimiento+"</td>";
					tabla +="<td>"+v.telefono+"</td>";
					tabla +="<td>"+v.celular+"</td>";
					tabla +="<td>"+v.ciudad+"</td>";
					tabla +="<td>"+v.direccion+"</td>";
					tabla +="<td><div class='btn-group'><a href='#'' data-toggle='dropdown' class='btn btn-primary dropdown-toggle'>Opciones <span class='caret'></span></a>";
					tabla +="<ul class='dropdown-menu' role='menu'>";
					tabla +="<li><a href='#' onclick='update_cliente("+v.codigo_cliente+"')>Modificar</a></li> <li><a href='#' onclick='delete_cliente("+v.codigo_cliente+")'>Eliminar</a></li>";
					tabla +="</ul> </div> </td> </tr>";
				});
	
				$("#DataTables_Table_0 tbody").append(tabla);
				$(".datatable").dataTable();
			}
		})
	})
}*/
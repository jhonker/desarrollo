var AJ_AX='';
$(document).ready(function(){
	get_tabla();
	$("#cbm_seguros").val(1);
	get_resul_seguro(1);
});

$("#btn_new_venta").click(function(){
	redirect('ventas/new');
});

$("#save_datos").click(function(){
	var telefono = $("#telefono_e").val();
	var celular = $("#celular_e").val();
	var correo = $("#correo_e").val();
	var token   = $("#token").val();
	var codigo = $("#codigo").html();
	$.ajax({
	url:'/update_datos_cliente',
	type:'POST',
	dataType:'json',
	headers :{'X-CSRF-TOKEN': token},
	data:{
		codigo:codigo,
		telefono:telefono,
		celular:celular,
		correo:correo
	},
	success:function(res){
		if(res.RES){
			alert("Datos Actualizados");
			$("#sp_telefono").html('<span id="telefono">'+telefono+'</span>');
			$("#sp_celular").html('<span id="celular">'+celular+'</span>');
			$("#sp_correo").html('<span id="correo">'+correo+'</span>');
			$("#edit_datos").show();
			$("#save_datos").hide();
		}
	}
	})
});
$("#edit_datos").click(function(){
var telefono = $("#telefono").html();
var celular = $("#celular").html();
var correo = $("#correo").html();

$("#sp_telefono").html('<input type="text" id="telefono_e" class="form-control" value="'+telefono+'">');
$("#sp_celular").html('<input type="text" id="celular_e" class="form-control" value="'+celular+'">');
$("#sp_correo").html('<input type="text" id="correo_e" class="form-control" value="'+correo+'">');
$("#edit_datos").hide();
$("#save_datos").show();

});


$("#btn_buscar_cliente").click(function(){
	//$("#modal_cliente_search").modal("show");

	searchClient()
});

$("#nombres").keyup(function(){
	//$("#modal_cliente_search").modal("show");
	if(this.value.length > 5){
		searchClient()
	}else{
		$("#div-search-hd").hide()
	}
	
});

function searchClient(){
	var token   = $("#token").val();
	if(AJ_AX && AJ_AX.readyState != 4){
		AJ_AX.abort();
		AJ_AX = null;
	  }
	  AJ_AX = $.ajax({
		url:'/clientes/listado-cliente-search',
		type:'POST',
		dataType:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			search:$("#nombres").val()

		},
		success:function(res){
			var ht ='';
			if(res){
				ht ='<table class="table">'
				$(res).each(function(i, v){
					ht +="<tr>";
					ht +="<td>"+v.numero_identificacion+"</td>";
					ht +="<td>"+v.nombres+"</td>";
					ht +="<td><button type='button' class='btn btn-info' onclick='seleccionar("+v.codigo_cliente+")'>Seleccionar Usuario</button> </td> </tr>";
				});
				ht +="</tbody> </table>";
				$("#div-search-hd").html(ht);
				$("#div-search-hd").show()
				
			}else{
				swal("El Cliente no se encontro", "error");
				$("#div-search-hd").hide()
			}
			
		}
	})
}

//seleccionar(10085)

$("#cbm_seguros").change(function(){
	get_resul_seguro($(this).val());
});
$("#btn_registrar").click(function(){
var n_identificacion = $("#identificacion").html();
var correo = $("#correo").html();
var nombres 	 	 = $("#nombres").val();
var codigo_cuenta 	 = $("#codigo_cuenta").val();
var token   = $("#token").val();
var nfila = $("#tabla_dependencia  tbody").find("tr").length; 
var array_dep = [];
      for(var x=1; x<=nfila ; x++){
        var obj = {
          'numero_identificacion': $("#t_identificacion"+x).html(),
          'nombres': $("#nombres"+x).html(),
          'fecha_nacimiento': $("#f_naci"+x).html(),
          'genero': $("#genero"+x).html(),
          'parentezco': $("#parentesco"+x).html(),
        }
        array_dep.push(obj);
      }// fin del for
var depenencia= JSON.stringify(array_dep);
	if(codigo_cuenta.length == 0 )
	{
		$("#cbm_cuentas").focus();
		alert("Seleccionar cuenta!");
	}else{
		$.ajax({
			url:'/store_venta',
			type:'POST',
			dataType:'json',
			headers :{'X-CSRF-TOKEN': token},
			data:{
				n_identificacion:n_identificacion,
				nombres:nombres,
				codigo_cuenta:codigo_cuenta,
				depenencia:depenencia,
				correo:correo
			},
			success:function(res){
				if(res.RES){
					alert("venta registrada");
					redirect('/ventas');
				}
			}
		})
	}
});
$("#cantidad_seguro").keyup(function(){
	

})
function get_resul_seguro(id){
	$.ajax({
		url:'/get_resul_seguro/'+id,
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				$("#precio_seguro").val(v.valor);
			});
		}
	})
}

$("#cbm_cuentas").change(function(){
	var codigo_cuenta=$(this).val();
	$.ajax({
		url:'/get_tipo_cuenta/'+codigo_cuenta,
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				$("#tipo_cuenta").html(v.tipo_cuenta+" - "+codigo_cuenta);
				$("#codigo_cuenta").val(codigo_cuenta);
			});
		}
	})
});

$("#btn_select_añadir").click(function(){
	alert($("#cbm_seguros").val());
})

function redirect(url){window.location=url; }
function get_tabla(){
	var tabla="<table class='table datatable' ><thead><tr><th>IDENTIFICACIÓN</th><th>NOMBRES</th><th></th></tr></thead> <tbody>"
	$.ajax({
		url:'/clientes/listado-cliente',
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				tabla +="<tr>";
				tabla +="<td>"+v.numero_identificacion+"</td>";
				tabla +="<td>"+v.nombres+"</td>";
				tabla +="<td><button type='button' class='btn btn-info' onclick='seleccionar("+v.codigo_cliente+")'>Seleccionar Usuario</button> </td> </tr>";
			});
			tabla +="</tbody> </table>";

			$("#tabla_cliente").html(tabla);
			$(".datatable").dataTable();

		}
	})
}

function seleccionar(id){
	$("#div-search-hd").hide()
	$.ajax({
		url:'/select_cliente/'+id,
		type:'GET',
		dataType:'json',
		success:function(res){
			$("#codigo").html(res.id);
			$("#nombres").val(res.nombres);
			$("#identificacion").html(res.identificacion);
			$("#telefono").html(res.telefono);
			$("#correo").html(res.correo);
			$("#celular").html(res.celular);
			get_cuentas(res.identificacion);
			$("#info").show();
			$("#modal_cliente_search").modal("hide");
		}

	})
}

function get_cuentas(identificacion){

	var combo="<option value='0'>Selecciones la cuenta</option>";
	$.ajax({
		url:'/get_cuentas/'+identificacion,
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				combo+="<option value='"+v.codigo_cuenta+"'>"+v.nombre_cuenta+"</option>";
			});
				$("#cbm_cuentas").html(combo);
				
		}

	})
}



$("#btn_exportar").click(function(){
	$("#modal_exportar").modal("show");
})

$("#b_añadir").click(function(){
	var identificacion = $("#identificacion_dep").val();
	var nombres = $("#nombres_dep").val();
	var f_nacimiento = $("#txt-date").val();
	var genero = $("#genero_dep").val();
	var parentesco =$("#parentesco").val();
	if(identificacion=="" && nombres=="" && f_nacimiento=="" &&genero==0 &&parentesco==0){
		alert("No se pudo añadir depenencia campos vacios");
		$("#identificacion_dep").focus();
	}else if(identificacion==""){
		$("#identificacion_dep").focus();
	}else if(nombres==""){
		$("#nombres_dep").focus();
	}else if(f_nacimiento==""){
 		$("#txt-date").focus();
	}else if(genero==0){

	}else if(parentesco==0){

	}else{
	 nfila = $("#tabla_dependencia  tbody").find("tr").length; // contamos los numero de filas (tr)
     nfila = nfila + 1;
    var y = 0;
    for (var x = 1; x <= nfila; x++) {
        if ($("#t_identificacion" + x + "").val() == identificacion) {
                cant_ = parseFloat($('#Tcantidad' + x + '').val()) + 1;
                y = y + 1;
            }
        } //FIN DEL FOR

   		if (y == 0) {
            cadena = "<tr>";
            cadena = cadena + "<td>" + nfila + "</td>";
            cadena = cadena + "<td> <span id='t_identificacion"+nfila+"'>"+identificacion+"</span></td>";
            cadena = cadena + "<td> <span id='nombres"+nfila+"'>" + nombres + "</span></td>";
            cadena = cadena + "<td> <span id='f_naci"+nfila+"'>" + f_nacimiento + "</span></td>";
            cadena = cadena + "<td> <span id='genero"+nfila+"'>" + genero+ " </span> </td>";
            cadena = cadena + "<td> <span id='parentesco"+nfila+"'>" + parentesco+ " </span> </td>";
            cadena = cadena + "<td><a class='elimina'><img class='delete' src='/dist/remove.png' /></a></td>";
            $("#tabla_dependencia tbody").append(cadena);

            $("#identificacion_dep").val("");
	 		$("#nombres_dep").val("");
	 		$("#txt-date").val("");
	 		$("#genero_dep").val(0);
			$("#parentesco").val(0);
        }else{
            alert("El pariente: "+nombres +" ya esta agregado");
        }

        fn_dar_eliminar();
	}

})

function fn_dar_eliminar() {
    $("a.elimina").click(function() {
        id = $(this).parents("tr").find("td").eq(0).html();
        	$(this).parents("tr").fadeOut("normal", function() {
             	$(this).remove();
            });
    });
};

$("#b_añadir_2").click(function(){
	var identificacion = $("#identificacion_dep").val();
	var nombres = $("#nombres_dep").val();
	var f_nacimiento = $("#txt-date").val();
	var genero = $("#genero_dep").val();
	var parentesco =$("#parentesco").val();
	var titular = $("#cedula_titular").val();
	if(identificacion=="" && nombres=="" && f_nacimiento=="" &&genero==0 &&parentesco==0){
		alert("No se pudo añadir depenencia campos vacios");
		$("#identificacion_dep").focus();
	}else if(identificacion==""){
		$("#identificacion_dep").focus();
	}else if(nombres==""){
		$("#nombres_dep").focus();
	}else if(f_nacimiento==""){
 		$("#txt-date").focus();
	}else if(genero==0){

	}else if(parentesco==0){

	}else{
	AddDependientes(titular,identificacion,nombres,f_nacimiento,genero,parentesco);
	/* nfila = $("#tabla_dependencia  tbody").find("tr").length; // contamos los numero de filas (tr)
     nfila = nfila + 1;
    var y = 0;
    for (var x = 1; x <= nfila; x++) {
        if ($("#t_identificacion" + x + "").val() == identificacion) {
                cant_ = parseFloat($('#Tcantidad' + x + '').val()) + 1;
                y = y + 1;
            }
        } //FIN DEL FOR

   		if (y == 0) {
            cadena = "<tr>";
            cadena = cadena + "<td>" + nfila + "</td>";
            cadena = cadena + "<td> <span id='t_identificacion"+nfila+"'>"+identificacion+"</span></td>";
            cadena = cadena + "<td> <span id='nombres"+nfila+"'>" + nombres + "</span></td>";
            cadena = cadena + "<td> <span id='f_naci"+nfila+"'>" + f_nacimiento + "</span></td>";
            cadena = cadena + "<td> <span id='genero"+nfila+"'>" + genero+ " </span> </td>";
            cadena = cadena + "<td> <span id='parentesco"+nfila+"'>" + parentesco+ " </span> </td>";
            cadena = cadena + "<td><a class='elimina'><img class='delete' src='/dist/remove.png' /></a></td>";
            $("#tabla_dependencia tbody").append(cadena);

            $("#identificacion_dep").val("");
	 		$("#nombres_dep").val("");
	 		$("#txt-date").val("");
	 		$("#genero_dep").val(0);
			$("#parentesco").val(0);
        }else{
            alert("El pariente: "+nombres +" ya esta agregado");
        }

        fn_dar_eliminar();*/
	}
});

function AddDependientes(titular,identificacion,nombres,f_nacimiento,genero,parentesco){
	var token   = $("#token").val();
	$.ajax({
		url:'AddDependientes',
		type:'POST',
		headers :{'X-CSRF-TOKEN': token},
		dataType:'json',
		data:{
			titular:titular,
			identificacion:identificacion,
			nombres:nombres,
			f_nacimiento:f_nacimiento,
			genero:genero,
			parentesco:parentesco,
		},
		success:function(res){
			if(res.RES){
					get_tabla_dependientes(titular);
					limpiar_dependientes();
					}
		}
	})
}

function get_tabla_dependientes(titular){
	$.ajax({
		url:'get_dependientes/'+titular,
					type:'GET',
					dataType:'json',
					success:function(res){
					var tbody1 = "";
					$(res).each(function(i, v){
						tbody1 +="<tr><td>"+i+"</td><td>"+v.numero_identificacion+"</td>";
						tbody1 +="<td>"+v.nombres+"</td>";
						tbody1 +="<td>"+v.fecha_nacimiento+"</td>";
						tbody1 +="<td>"+v.genero+"</td>";
						tbody1 +="<td>"+v.parentezco+"</td><td><a class='elimina' onclick='deleteDependiente("+v.codigo_cliente_dependiente+")'><img class='delete' src='/dist/remove.png' /></a><td></tr>";
					});
					$("#tbody_dependientes").html(tbody1);
				}
			})
}

function limpiar_dependientes(){
	 $("#identificacion_dep").val(""); $("#nombres_dep").val(""); $("#txt-date").val(""); $("#genero_dep").val(0); $("#parentesco").val(0);
}
function add_dependientes(cedula){
	$("#cedula_titular").val(cedula);
	$.ajax({
		url:'get_dependientes/'+cedula,
		type:'GET',
		dataType:'json',
		success:function(res){

			var tbody1 = "";
			console.log(res.length);

			$(res).each(function(i, v){
				tbody1 +="<tr><td>"+i+"</td><td>"+v.numero_identificacion+"</td>";
				tbody1 +="<td>"+v.nombres+"</td>";
				tbody1 +="<td>"+v.fecha_nacimiento+"</td>";
				tbody1 +="<td>"+v.genero+"</td>";
				tbody1 +="<td>"+v.parentezco+"</td><td><a class='elimina' onclick='deleteDependiente("+v.codigo_cliente_dependiente+")'><img class='delete' src='/dist/remove.png'/></a><td></tr>";
			});
			console.log(tbody1);
			$("#tbody_dependientes").html(tbody1);
			$("#modal_agregar_dependientes").modal("show");
		}
	})
}


function deleteDependiente(id){
	var titular = $("#cedula_titular").val();
	var token   = $("#token").val();
	swal({ title: "LAB-Palacios",
      text: "Esta Seguro de Eliminar este dependiente!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#4CAF50",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: true,
      closeOnCancel: true},
      function(isConfirm){
        if (isConfirm) {
        	$.ajax({
        		url:'delete_dependiente',
        		type:'POST',
        		dataType:'json',
				headers :{'X-CSRF-TOKEN': token},
        		data:{
        			id:id
        		},
        		success:function(res){
        			if(res.RES){
        				//get_tabla();
						get_tabla_dependientes(titular);
        				swal("El dependiente a sido eliminado", "success");
        			}
        		}
        	})
        }
    });
}



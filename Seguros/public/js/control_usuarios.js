$("#btn_new_usuario").click(function(){
	$("#modal_usuarios").modal("show");
})



function delete_usuario(id){
var token   = $("#token").val();
	swal({ title: "LAB-Palacios",
      text: "Esta Seguro de Eliminar este usuario!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#4CAF50",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: true,
      closeOnCancel: true},
      function(isConfirm){
        if (isConfirm) {
        	$.ajax({
        		url:'delete_usuario',
        		type:'POST',
        		dataType:'json',
				headers :{'X-CSRF-TOKEN': token},
        		data:{
        			id:id
        		},
        		success:function(res){
        			if(res.RES){
        				get_tabla();
        				swal("El usuario a sido eliminado", "success");
        			}
        		}
        	})
        }
    });
}


$("#btn_store_usuario").click(function(){
	var rol = $("#cbm_rol").val();
	var nombre_usuario =$("#nombre_usuario").val();
	var nombres = $("#nombres").val();
	var p_apellido = $("#primer_apellido").val();
	var s_apellido = $("#segundo_apellido").val();
	var contraseña = $("#pass").val();
	var token   = $("#token").val();


	if(rol=="0" && nombre_usuario=="" && nombres=="" && p_apellido=="" && s_apellido=="" && contraseña==""){
		alert("Los campos se encuentran vacios");
		return false;
	}else if(rol=="0"){
		alert("Por favor seleccione un rol");
		return false;
	}else if(nombre_usuario==""){
		alert("Por favor ingrese un nombre de usuario");
		return false;
	}else if(nombres==""){
		alert("Por favor ingrese los nombres");
		return false;
	}else if(p_apellido==""){
		alert("Por favor ingrese el primer apellido");
		$("#primer_apellido").focus();
		return false;
	}else if(s_apellido==""){
		alert("Por favor ingrese el segundo apellido");
		$("#segundo_apellido").focus();
		return false;
	}else if(contraseña==""){
		alert("Por favor ingrese una contraseña");
		$("#pass").focus();
		return false;
	}else{
		store_usuarios(rol,nombre_usuario,nombres,p_apellido,s_apellido,contraseña,token);
	}
});


function store_usuarios(rol,nombre_usuario,nombres,p_apellido,s_apellido,contraseña,token){
	$.ajax({
		url:'usuarios',
		type:'POST',
		dataType:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			rol:rol,
			nombre_usuario:nombre_usuario,
			nombres:nombres,
			p_apellido:p_apellido,
			s_apellido:s_apellido,
			pass:contraseña
		},
		success:function(res){
			if(res.RES){
				alert("Usuario registrado");
				$("#modal_usuarios").modal("hide");
				get_tabla();
			}
		}
	});
}

function get_tabla(){
	var tabla=" <table class='table datatable'> <thead> <tr> <th>CODIGO</th> <th>NOMBRE DE USUARIO</th> <th>USUARIO</th> <th>OPCIONES</th> </tr> </thead> <tbody>"
	$.ajax({
		url:'get_usuarios',
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				tabla +="<tr>";
				tabla +="<td>"+v.codigo_usuario+"</td>";
				tabla +="<td>"+v.usuario+"</td>";
				tabla +="<td>"+v.nombres+" "+v.primer_apellido+" "+v.segundo_apellido+"</td>";
				tabla +="<td><div class='btn-group'><a href='#'' data-toggle='dropdown' class='btn btn-primary dropdown-toggle'>Opciones <span class='caret'></span></a>";
				tabla +="<ul class='dropdown-menu' role='menu'>";
				tabla +="<li><a href='#' onclick='update_usuario("+v.codigo_usuario+")'>Modificar</a></li> <li><a href='#' onclick='delete_usuario("+v.codigo_usuario+")'>Eliminar</a></li>";
				tabla +="</ul> </div> </td> </tr>";
			});
			tabla +="</tbody> </table>";

			$("#tabla_usuarios").html(tabla);
			$(".datatable").dataTable();
		}
	})
}


function update_usuario(id){
		$.ajax({
			url:'usuario/edit/'+id,
			type:'GET',
			dataType:'json',
			success:function(res){
				if(res.RES){
					$("#id_usuario").val(id);
				 	$("#cbm_rol_a").val(res.rol);
				 	$("#nombre_usuario_a").val(res.usuario);
				 	$("#nombres_a").val(res.nombres);
				 	$("#primer_apellido_a").val(res.p_apellido);
				 	$("#segundo_apellido_a").val(res.s_apellido);
					$("#modal_usuarios_update").modal("show");
				}
			}
		})
}

$("#btn_update_usuario").click(function(){
	var id = $("#id_usuario").val();
	var codigo_rol = $("#cbm_rol_a").val();
	var usuario = $("#nombre_usuario_a").val();
	var nombres = $("#nombres_a").val();
	var p_apellido = $("#primer_apellido_a").val();
	var s_apellido = $("#segundo_apellido_a").val();
	var token   = $("#token").val();

	$.ajax({
		url:'update_usuario',
		type:'POST',
		dataType:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			id:id,
			codigo_rol:codigo_rol,
			usuario:usuario,
			nombres:nombres,
			p_apellido:p_apellido,
			s_apellido:s_apellido
		},
		success:function(res){
			if(res.RES){
				$("#modal_usuarios_update").modal("hide");
				alert("Actualizacion Correcta");
				get_tabla();

			}
		}

	})

})



function redirect(url){window.location=url; }
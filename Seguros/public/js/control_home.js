$(document).ready(function() {
    get_n_cliente();
});

function get_n_cliente(){
$.ajax({
    url:'get_count_clientes',
    type:'GET',
    dataType:'json',
    success:function(res){
      $(res).each(function(i, v){
        $("#count_user").html(v.c);
      });
    }
});

$.ajax({
    url:'get_count_totales_nuevos',
    type:'GET',
    dataType:'json',
    success:function(res){
      $(res).each(function(i, v){
        $("#count_seguros").html(v.count);
      });
    }
});

$.ajax({
    url:'get_count_totales_cobrados_mensual',
    type:'GET',
    dataType:'json',
    success:function(res){
      $(res).each(function(i, v){
        $("#count_mensual").html(v.count);
      });
    }
});

$.ajax({
    url:'get_count_totales_cancelados',
    type:'GET',
    dataType:'json',
    success:function(res){
      $(res).each(function(i, v){
        $("#count_cancelados").html(v.count);
      });
    }
});

$.ajax({
    url:'get_count_totales_activos',
    type:'GET',
    dataType:'json',
    success:function(res){
      $(res).each(function(i, v){
        $("#count_activos").html(v.count);
      });
    }
});

$.ajax({
    url:'get_periodo',
    type:'GET',
    dataType:'json',
    success:function(res){
      $(res).each(function(i, v){
        $("#div_periodo").html(v.mes_anio_cobrar+', '+v.mes_anio_siguiente);
      });
    }
});


// view_lb_totales_clientes,  
}

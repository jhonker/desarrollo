$("#btn_new_rol").click(function(){
	$("#modal_roles").modal("show");
});


$("#btn_store_rol").click(function(){
	var rol = $("#rol").val();
	var token   = $("#token").val();

	if(rol==""){
		alert("Por favor ingrese el rol");
	}else{
		store_rol(rol,token);
	}
});	

function get_tabla(){
	var tabla=" <table class='table datatable'> <thead> <tr> <th>CODIGO</th> <th>ROL</th> <th>FECHA CREADO</th> <th>MODIFICADO</th> <th>OPCIONES</th> </tr> </thead> <tbody>"
	$.ajax({
		url:'get_roles',
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				tabla +="<tr>";
				tabla +="<td>"+v.codigo_rol+"</td>";
				tabla +="<td>"+v.rol+"</td>";
				tabla +="<td>"+v.fecha_adicion+"</td>";
				tabla +="<td>"+v.usuario_adicion+"</td>";
				tabla +="<td><div class='btn-group'><a href='#'' data-toggle='dropdown' class='btn btn-primary dropdown-toggle'>Opciones <span class='caret'></span></a>";
				tabla +="<ul class='dropdown-menu' role='menu'>";
				tabla +="<li><a href='#' onclick='update_rol("+v.codigo_rol+")'>Modificar</a></li> <li><a href='#' onclick='delete_rol("+v.codigo_rol+")'>Eliminar</a></li>";
				tabla +="</ul> </div> </td> </tr>";
			});
			tabla +="</tbody> </table>";

			$("#tabla_rol").html(tabla);
			$(".datatable").dataTable();
		}
	})
}


function store_rol(rol,token){
	$.ajax({
		url:'roles',
		type:'POST',
		dataType:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			rol:rol
		},
		success:function(res){
			if(res.RES){
				get_tabla();
				$("#modal_roles").modal("hide");
				$("#rol").val("");

			}
		}
	})
}

function delete_rol(id){
	var token   = $("#token").val();
	swal({ title: "LAB-Palacios",
      text: "Esta Seguro de Eliminar este ROL!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#4CAF50",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: true,
      closeOnCancel: true},
      function(isConfirm){
        if (isConfirm) {
        	$.ajax({
        		url:'delete_rol',
        		type:'POST',
        		dataType:'json',
				headers :{'X-CSRF-TOKEN': token},
        		data:{
        			id:id
        		},
        		success:function(res){
        			if(res.RES){
        				get_tabla();
        				swal("El rol a sido eliminado", "success");
        			}
        		}
        	})
        }
    });
}

function update_rol(id){
	$.ajax({
		url:'roles/'+id,
		type:'GET',
		dataType:'json',
		success:function(res){
			if(res.RES){
				$("#id_rol").val(id);
				$("#rol_A").val(res.rol);
				$("#modal_roles_update").modal("show");
			}
		}
	})
}

$("#btn_update_rol").click(function(){
	var token   = $("#token").val();
	var rol=$("#rol_A").val();
	var id=$("#id_rol").val();
	$.ajax({
		url:'update_rol',
		type:'POST',
		dataType:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			id:id,
			rol:rol
		},
		success:function(res){
			if(res.RES){
				$("#modal_roles_update").modal("hide");
				get_tabla();
			}
		}
	})

});
$(document).ready(function(){
	$("#usuario").focus();
});

$("#pass").keypress(function(tecla) {
  if (tecla.which == 13) {
  	$("#btn_iniciar").html('<i class="fa fa-spinner fa-spin fa-fw"></i>')
	$("#btn_iniciar").attr('disabled',true)
   var usuario = $("#usuario").val();
	var pass	= $("#pass").val();
	var token   = $("#token").val();

	if(usuario=="" && pass==""){
		alert("Por favor ingrese sus datos");
	}else if(usuario==""){
		alert("Por favor ingrese su nombre de usuario");
		$("#usuario").focus();
	}else if(pass==""){
		alert("Por favor ingrese la contraseña");
		$("#pass").focus();
	}else{
		login(usuario,pass,token);
	}
  }
});

$("#btn_iniciar").click(function(){
	$("#btn_iniciar").html('<i class="fa fa-spinner fa-spin fa-fw"></i>')
	$("#btn_iniciar").attr('disabled',true)
	var usuario = $("#usuario").val();
	var pass	= $("#pass").val();
	var token   = $("#token").val();

	if(usuario=="" && pass==""){
		alert("Por favor ingrese sus datos");
	$("#btn_iniciar").html('Iniciar Sessión')
	$("#btn_iniciar").attr('disabled',false)
	}else if(usuario==""){
		alert("Por favor ingrese su nombre de usuario");
		$("#usuario").focus();
		$("#btn_iniciar").html('Iniciar Sessión')
		$("#btn_iniciar").attr('disabled',false)
	}else if(pass==""){
		alert("Por favor ingrese la contraseña");
		$("#pass").focus();
		$("#btn_iniciar").html('Iniciar Sessión')
		$("#btn_iniciar").attr('disabled',false)
	}else{
		login(usuario,pass,token);
	}
});



function login(usuario,pass,token){
	$.ajax({url:'login', type:'POST', dataType:'json', headers :{'X-CSRF-TOKEN': token}, data:{usuario:usuario, pass:pass },
		success:function(res){
			if(res.sms){
                redirect('home');
			}else{
				$("#btn_iniciar").html('Iniciar Sessión')
				$("#btn_iniciar").attr('disabled',false)
			}
		}
	})
}

function redirect(url){window.location=url; }
$("#btn_new_seguro").click(function(){
	$("#modal_seguro").modal("show");
})

$("#mes_cobro").change(function(){
	var anio_ant= $(this).val().substring(3, 7);
	var anio_sig= $(this).val().substring(3, 7);
	var mes = $(this).val().substring(0, 2);
	var mes_ant = (parseInt(mes)-1);
	var mes_sig = (parseInt(mes)+1);
	if(mes=="12"){
		mes_sig = "1";
		anio_sig=(parseInt(anio_sig)+1);
	}else if(mes=="01"){
		mes_ant="12";
		anio_ant=(parseInt(anio_ant)-1);
	}
	if(mes_ant>=10){
		$("#mes_anterior").val(mes_ant+"-"+anio_ant);
	}else{
		$("#mes_anterior").val("0"+mes_ant+"-"+anio_ant);
	}

	if(mes_sig>=10){
		$("#mes_sig").val(mes_sig+"-"+anio_sig)
	}else{
		$("#mes_sig").val("0"+mes_sig+"-"+anio_sig)
	}
})

$("#mes_cobro_a").change(function(){
	var anio_ant= $(this).val().substring(3, 7);
	var anio_sig= $(this).val().substring(3, 7);
	var mes = $(this).val().substring(0, 2);
	var mes_ant = (parseInt(mes)-1);
	var mes_sig = (parseInt(mes)+1);
	if(mes=="12"){
		mes_sig = "1";
		anio_sig=(parseInt(anio_sig)+1);
	}else if(mes=="01"){
		mes_ant="12";
		anio_ant=(parseInt(anio_ant)-1);
	}
	if(mes_ant>=10){
		$("#mes_anterior_a").val(mes_ant+"-"+anio_ant);
	}else{
		$("#mes_anterior_a").val("0"+mes_ant+"-"+anio_ant);
	}

	if(mes_sig>=10){
		$("#mes_sig_a").val(mes_sig+"-"+anio_sig)
	}else{
		$("#mes_sig_a").val("0"+mes_sig+"-"+anio_sig)
	}
})


$("#btn_store_seguro").click(function(){
	var seguro 		= $("#descripcion").val();
	var valor  	  	= $("#valor").val();
	var mes_cobro 	= $("#mes_cobro").val();
	var mes_anterior= $("#mes_anterior").val();
	var mes_sig		= $("#mes_sig").val();
	var max_atraso	= $("#max_atraso").val();
	var token   = $("#token").val();


	if(seguro=="" && valor=="" && mes_cobro=="" && mes_anterior=="" && mes_sig=="" && max_atraso==""){

	}else if(seguro==""){

	}else if(valor=="" && valor=="0"){

	}else if(mes_cobro==""){

	}else if(mes_anterior==""){

	}else if(mes_sig==""){

	}else if(max_atraso==""){

	}else{
		strore_seguro(seguro,valor,mes_cobro,mes_anterior,mes_sig,max_atraso,token);
	}

});	

function strore_seguro(seguro,valor,mes_cobro,mes_anterior,mes_sig,max_atraso,token){
	$.ajax({
		url:'seguros',
		type:'POST',
		datatype:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			seguro:seguro,
			valor:valor,
			mes_cobro:mes_cobro,
			mes_anterior:mes_anterior,
			mes_sig:mes_sig,
			max_atraso:max_atraso
		},
		success:function(res){
			if(res.RES){
				alert("Seguro registrado");
				$("#modal_seguro").modal("hide");
				get_tabla();
			}
		}
	});
}

function update_seguro(id) {
	$.ajax({
		url:'seguros/'+id,
		type:'GET',
		dataType:'json',
		success:function(res){
			if(res.RES){
				$("#id_seguro").val(id);
				$("#descripcion_a").val(res.descripcion);
				$("#valor_a").val(res.valor);
				$("#mes_cobro_a").val(res.mes_anio_cobrar);
				$("#mes_anterior_a").val(res.mes_anio_ant);
				$("#mes_sig_a").val(res.mes_anio_siguiente);
				$("#modal_seguro_update").modal("show");
			}
		}
	})
}

function delete_seguro(id){
	var token   = $("#token").val();
	swal({ title: "LAB-Palacios",
      text: "Esta Seguro de Eliminar este este seguro!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#4CAF50",
      confirmButtonText: "Si!",
      cancelButtonText: "No!",
      closeOnConfirm: true,
      closeOnCancel: true},
      function(isConfirm){
        if (isConfirm) {
        	$.ajax({
        		url:'seguros_delete',
        		type:'POST',
        		dataType:'json',
				headers :{'X-CSRF-TOKEN': token},
        		data:{
        			id:id
        		},
        		success:function(res){
        			if(res.RES){
        				get_tabla();
    					swal("LAB-Palacios!", "Seguro eliminado.", "success");   
        			}
        		}
        	})
        }
    });
}

function get_tabla(){
	var tabla=" <table class='table datatable'> <thead> <tr> <th>CODIGO</th> <th>DESCRIPCION</th> <th>PRECIO</th> <th>MES DE COBRO</th> <th>MAXIMO ATRASO</th> <th>OPCIONES</th> </tr> </thead> <tbody>"
	$.ajax({
		url:'get_seguros',
		type:'GET',
		dataType:'json',
		success:function(res){
			$(res).each(function(i, v){
				tabla +="<tr>";
				tabla +="<td>"+v.codigo_seguro+"</td>";
				tabla +="<td>"+v.descripcion+"</td>";
				tabla +="<td>"+v.valor+"</td>";
				tabla +="<td>"+v.mes_anio_cobrar+"</td>";
				tabla +="<td>"+v.maximo_atraso+"</td>";
				tabla +="<td><div class='btn-group'><a href='#'' data-toggle='dropdown' class='btn btn-primary dropdown-toggle'>Opciones <span class='caret'></span></a>";
				tabla +="<ul class='dropdown-menu' role='menu'>";
				tabla +="<li><a href='#' onclick='update_rol("+v.codigo_seguro+")'>Modificar</a></li> <li><a href='#' onclick='delete_rol("+v.codigo_seguro+")'>Eliminar</a></li>";
				tabla +="</ul> </div> </td> </tr>";
			});
			tabla +="</tbody> </table>";

			$("#tabla_seguro").html(tabla);
			$(".datatable").dataTable();
		}
	})
}


$("#btn_update_seguro").click(function(){
	var token   = $("#token").val();
	var id = $("#id_seguro").val();
	var mes_cobro = $("#mes_cobro_a").val();
    var mes_ant = $("#mes_anterior_a").val();
	var mes_sig =$("#mes_sig_a").val();
	$.ajax({
		url:'seguro_update',
		type:'POST',
		dataType:'json',
		headers :{'X-CSRF-TOKEN': token},
		data:{
			id:id,
			mes_cobro:mes_cobro,
			mes_ant:mes_ant,
			mes_sig:mes_sig,
		},
		success:function(res){
			if(res.RES){
				$("#modal_seguro_update").modal("hide");
				get_tabla();
			}
		}
	})

});